---
layout:     page
title:      Pubblicazioni
h2:         
categories: pubblicazioni
slug:       pubblicazioni
---

<section class="pubblicazioni">

<article class="post" itemscope itemtype="http://schema.org/BlogPosting">
    <h2>
        <a class="u-url" href="https://www.globalist.it/intelligence/2018/09/19/sicurezza-della-internet-of-things-e-dell-intelligenza-artificiale/" target="out">
            Sicurezza della Internet Of Things e dell'intelligenza artificiale
        </a>
     </h2>
     <p class="sinossi">
        Come scoprire le vulnerabilità della Internet Of Things e dell'intelligenza artificiale studiando la smart-home dell’agente K di Blade Runner 2049
    </p>
    <p class="data">
        <b>Globalist.it</B> - 19 Settembre 2018 
    </p>
</article>

<article class="post" itemscope itemtype="http://schema.org/BlogPosting">
    <h2>
        <a class="u-url" 
            href="https://formiche.net/2021/06/ransomware-colonial/" 
            target="out">    
            Il fattore umano nel ransomware        
        </a>
     </h2>
    <p class="sinossi">
        Come mettere sotto scacco interi sistemi informatici sfruttando la distrazione e l’errore umano. Più che comprare tonnellate di software di difesa, la strada migliore è insegnare alle persone come comportarsi.
    </p>
    <p class="data">
        <b>Formiche</b> - 1 Giugno 2021
    </p>
</article>

<article class="post" itemscope itemtype="http://schema.org/BlogPosting">
    <h2>
        <a class="u-url" 
            href="https://formiche.net/2022/05/icone-moderne-font-awesome/" 
            target="out">    
            Icone dei tempi
        </a>
     </h2>
    <p class="sinossi">
        Gli ultimi anni di storia racchiusi nei simboli “umanitari”
    </p>
    <p class="data">
        <b>Formiche</b> - 1 Giugno 2021
    </p>
</article>


<article class="post" itemscope itemtype="http://schema.org/BlogPosting">
    <h2>
        <a class="u-url" 
            href="https://www.difesaesicurezza.com/difesa-e-sicurezza/cybersecurity-wordpress-di-maio-e-miley-cyrus/" 
            target="out"> 
            Cybersecurity: WordPress, Di Maio e Miley Cyrus           
        </a>
     </h2>
    <p class="sinossi">
        Come i CMS hanno trasformato Internet in un campo minato
    </p>
    <p class="data">
        <b>Difesa & Sicurezza</b> - 2 Dicembre 2022
    </p>
</article>

<article class="post" itemscope itemtype="http://schema.org/BlogPosting">
    <h2>
        <a class="u-url" 
            href="https://formiche.net/2023/01/swascan-chatgpt-intelligenza-artificiale/" 
            target="out">  
            ChatGPT e la sicurezza nell’Intelligenza artificiale. Conversazione con Iezzi (Swascan)          
        </a>
     </h2>
    <p class="sinossi">
        Intelligenza artificiale, cybersicurezza e questione etica. Intervista a tutto tondo di Carlo Simonelli a Pierguido Iezzi, ceo di Swascan
    </p>
    <p class="data">
        <b>Formiche</b> - 8 Gennaio 2023
    </p>
</article>


</section>
<aside class="dx">
</aside>
