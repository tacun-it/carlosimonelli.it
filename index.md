---
category:   "site"
excerpt:    "Opinioni di Carlo Simonelli, informatico"
h1:         "Index"
h2:         "Ultimi articoli pubblicati"
image:      "/assets/img/thumbnail.png"
img-alt:    "Logo del sito carlosimonelli.it"
layout:     home
permalink:  "/index.html" 
tags:       [security, information technology, cybersecurity, IT, computers, software]
thumbnail:  "/assets/img/index.jpg"
title:      "Home"
---

<div style="font-style:italic;font-size:0.9rem;background:#eee;padding:10px">
Ero stanco di passare il tempo a controllare i file di log per bloccare gli IP che tentano di fare qualcosa di malandrino, così, ho deciso di convertire questo sito da Wordpress a <a href="https://jekyllrb.com/" target="jekill">Jekyll</a>.<br />  
Ci vorrà un po'; vi prego di scusare eventuali errori di impaginazione o link alle note che non funzionano.<br />  
Grazie,<br />  
C:<br />
</div>
