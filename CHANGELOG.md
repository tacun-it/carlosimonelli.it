## 1.2.10 (February 28, 2023)
  - Correzioni ninori all'articolo sull'anniversario della Charta

## 1.2.9 (February 28, 2023)
  - Correzioni ninori all'articolo sulla Charta

## 1.2.8 (February 28, 2023)
  - Aggiunto articolo per i tre anni della Rome Call

## 1.2.7 (February 14, 2023)
  - Aggiunti PDF testi relazioni

## 1.2.6 (February 13, 2023)
  - Aggiunta pagina con pubblicazioni
  - Aggiunta thumbnail ad articolo Flora
  - Aggiunto articolo Flora

## 1.2.5 (December 23, 2022)
  - Aggiunta chiave PGP e articolo su Di Maio
  - Completata revisione articolo sui CMS
  - Revisione articolo sui CMS

## 1.2.4 (November 22, 2022)
  - Completata prima stesura articolo su CMS
  - Redazione dell'articolo sui CMS

## 1.2.3 (November 20, 2022)
  - Redazione articolo su CMS

## 1.2.2 (October 27, 2022)
  - Revisione grafica degli articoli
  - Corretti errori nell'HEAD

## 1.2.1 (January 04, 2022)
  - Corrette pagine categoria e bio
  - Aggiornato articolo su C'hi++

## 1.2.0 (January 02, 2022)
  - Aggiunto l'articolo sul COVID
  - Uniformati i campi dell'intestazione degli articoli
  - Aggiornati gli articoli su Meeetic
  - Corrette le note di Siri
  - Merge branch 'directory-post' into redazione
  - Revisione testo Siri
  - Aggiornato articolo su degrado del software

## 1.1.0 (December 27, 2021)
  - Aggiornato articolo su Security Summit
  - Iniziato aggiornamento articoli
  - Modificata struttura della directory img
  - Aggiunto file google-analytics.html
  - Modificato il file di configurazione

## 1.0.1 (December 23, 2021)
  - Aggiornato config.yml
  - Aggiornata la directory bin
  - Aggiornato il testo di IA e crostate

## 1.0.0 (July 02, 2021)
  - Aggiunti i due articoli su Meetic
  - Aggiunto testo prima parte articolo su Meetic
  - corretti errori di battitura 
  - modificata immagine articolo su degrado SW

## 0.2.0 (May 31, 2021)
  - Merge remote-tracking branch 'origin/hotfix/h3' into rel/0.2.0
  - Correzioni su sito in linea
  - Aggiunto articolo su Covid
  - aggiunta chiave GPG
  - Aggiornate le note dell'articolo sugli Hacker
  - Corrette le note di AI e crostate
  - Modifiche al testo
  - Iniziata revisione delle note di AI e crostate
  - Aggiunta favicon
  - Modificata la nota di apertura

## 0.1.1 (November 17, 2020)
  - Aggiunto file di configurazione Apache

## 0.1.0 (November 17, 2020)
  - corretti gli stili delle pagine
  - aggiunta directory _conf
  - aggiunta immagine articolo Romantico a Milano
  - aggiunta nota alla home-page
