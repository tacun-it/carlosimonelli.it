---
category:   "sicurezza"
excerpt:    "I siti di incontri, sono un bene o un male? Sì"
h1:         "L'amore al tempo di Meetic - prima parte"
h2:         "I siti di incontri, sono un bene o un male? Sì."
layout:     post
image:      "/assets/img/2015/logo-meetic-motto.jpg"
thumbnail:  "/assets/img/2015/logo-meetic-motto.jpg"
img-alt:    "Il logo di Meetic"
permalink:  "/sicurezza/amore-al-tempo-di-meetic-prima-parte.html" 
tags:       [sicurezza, Meetic, dating online]
title:      "L'amore al tempo di Meetic - prima parte"
---

Lilly s'è arrabbiata e non mi parla più.  
Lilly è una donna che ho conosciuto sul sito di incontri *meetic.com* e s'è arrabbiata con me perché ho utilizzato una delle foto che lei ha pubblicato per verificare una mia ipotesi sui rischi connessi all'utilizzo dei *social-network*.
La mia idea era che, senza utilizzare nient'altro che un semplice browser, fosse possibile sfruttare le immagini di un sito per acquisire informazioni sulla persona che le ha pubblicate. Per accertarmene, ho preso una delle foto del suo profilo e l'ho inserita nel motore di ricerca per immagini di Google.
Per non violare la sua privacy (gli utenti di Meetic hanno tutti uno pseudonimo), ho utilizzato la foto di un paesaggio romano che si prestava particolarmente bene ai miei scopi:

- era del tutto anonima: si vedevano solo la sagoma di un pino controluce, lo spigolo di un palazzo e, sullo sfondo, la cupola di San Pietro; 

- la struttura dell'immagine, per aree distinte, era perfetta per una ricerca automatizzata;

- mi ricordavo di aver visto un'immagine simile altrove.

Pochi istanti di attesa e Google mi ha dato ragione: in un sito di aste d'arte, c'era un'immagine pressocché identica, scattata dalla fotografa canadese Lisa Roze:

<img class="illustrazione cornice"
    src="/assets/img/2015/lisa-roze.png"
    alt="La foto di Lisa Roze">

Quando ho raccontato questa cosa a Lilly, la sua risposta è stata piuttosto seccata:

> non va piu' di moda chiedere prima di fare le cose? ma immagino siano i rischi di quando entri nel mondo del dating online

Ho cercato di spiegarle le mie buone intenzioni, le ho detto come mai avessi usato quella foto, le ho assicurato di non aver fatto altre prove, ma non c'è stato nulla da fare: i miei messaggi non hanno più ottenuto risposta.

È, questa, una reazione che conosco piuttosto bene. Quando dici a un gruppo di programmatori che il loro sistema ha un "buco" di sicurezza, le reazioni possibili sono due: c'è chi lavora tutto il fine settimana per correggere l'errore e poi ti ringrazia pubblicamente sul suo sito Web e c'è chi, al contrario, ti risponde sospettoso e poi ignora la tua segnalazione o la corregge malamente. Non occorre essere esperti di informatica per capire quale dei due gruppi produca il software migliore.  
È sbagliato offendersi quando qualcuno ci fa notare un nostro errore: siamo esseri umani e a tutti càpita di sbagliare, non c'è niente di male; tant'è vero che la seconda prova che ho fatto è stata con la foto del MIO profilo su Meetic e mi ha portato dritto dritto al mio acconto su Linkedin, dove compaio con il mio vero nome e non con uno pseudonimo. Imbarazzante..  
Ma riprendiamo la frase di Lilly:

> non va piu' di moda chiedere prima di fare le cose? ma immagino siano i rischi di quando entri nel mondo del dating online

Sul chiedere prima di fare, le dò ragione, ma il fatto è che l'idea mi è venuta intorno alle due di notte e mi piaceva troppo per aspettare a metterla in pratica.
Se avessi pensato di fare la prova con la foto del mio profilo tutto questo non sarebbe successo, ma, come ho detto, erano le due di notte e io non ero precisamente al meglio delle mie facoltà mentali: avevo la sua foto davanti agli occhi e ho usato quella.  
La seconda parte della frase è inesatta: il problema non sono i siti di *dating*, ma i *social-network* in generale.
Anche *ICQ*, nei primi anni di questo secolo, era un sito di chat e incontri, ma allora era molto più difficile ottenere informazioni aggiuntive sulle persone che conoscevi; se non avevano un blog o un sito Web, era ben difficile trovare qualcosa.
Oggi, al contrario, ciascuno di noi, immette quotidianamente in Internet un'enorme mole di informazioni su di sé.
Facebook, Google+, LinkedIn, YouTube, MySpace, Instagram, Twitter, Tumblr, Badoo, Foursquare.. 
A quanti di questi siti siete iscritti? 
Quante foto e quanti testi avete pubblicato? 
Quante, di queste foto o di questi testi, contengono informazioni sulla vostra vita e sulle vostre abitudini?  
Il problema, quindi, non sono i *social-network*, ma il *proliferare* dei *social-network* e la scarsa attenzione che poniamo nel loro utilizzo. Commettere una leggerezza su un *social-network* può essere un errore veniale, senza conseguenze, ma se si disseminano tracce qua e là, aumenta il rischio che qualcuno possa fare due più due e arrivarci sotto casa o sotto l'ufficio con conseguenze tutt'altro che piacevoli.  
Tanto per essere chiari, i principali errori da evitare sono:

1. Non usare uno pseudonimo che sia l'unione del vostro nome e del vostro cognome. Non è una battuta: c'è chi lo fa, ed è come se James Bond usasse, come nome di copertura, Jamesbond..

2. Non pubblicare, nei siti in cui comparite sotto pseudonimo, le stesse foto che avete pubblicato altrove &mdash; con: "altrove" non intendo solo i social-network, ma qualsiasi altro sito Web che contenga informazioni su di voi;

3. Differenziare le amicizie sui *social-network* e limitare la visibilità delle immagini e delle informazioni pubblicate. Le foto e i post che contengono informazioni relative alla vostra vita privata al vostro lavoro o alle vostre abitudini devono essere visibili solo alle persone che conoscete bene. Vale anche per le foto dei vostri figli.

4. Separare il lavoro dalla vita privata. Se avete un'attività che vi porta a contatto con il pubblico e la volete pubblicizzare su Facebook, create una pagina distinta da quella dove mettete le foto e notizie personali. 

5. Ricordarsi che Google trova anche i numeri di telefono. La ricerca del vostro numero di telefono su Google può fornire delle informazioni su di voi: luogo di lavoro, beni posseduti ecc. Tenetene conto, prima di cominciare a scambiarvi SMS con qualcuno che non conoscete.

Mettere in pratica queste regole può sembrare complicato, ma non lo è.
Sicuramente è più facile che liberarsi di un corteggiatore oppressivo.
Lo vedremo nella <a href="/sicurezza/amore-al-tempo-di-meetic-seconda-parte.html" class="xref">seconda parte</a> di questo articolo.
