---
category:   "sicurezza"
h1:         "Da Parry il paranoico a ChatGPT"
h2:         "Note sull'emancipazione dell'intelligenza artificiale, tre anni dopo la “Rome Call for AI Ethics”"
layout:     post
image:      "/assets/img/2023/da-parry-a-chatgpt.jpg"
thumbnail:  "/assets/img/2023/da-parry-a-chatgpt-m.jpg"
img-alt:    "L'interfaccia di ChatGPT"
permalink:  "/informatica/da-parry-a-chatgpt.html" 
tags:       [intelligenza artificiale, AI, ChatGPT, iBorderCtr, algoetica, Vincenzo Paglia, Rome Call for AI Ethics]
title:      "Da Parry il paranoico a ChatGPT"
excerpt:    "Note sull'emancipazione dell'intelligenza artificiale, tre anni dopo la “Rome Call for AI Ethics”"
---


<section class="citazione" style="text-align:left">
- Ti racconto una barzelletta: Un calvinista, un francescano e un comunista entrano in un bar. Il barista chiede: «Cos'è, la ricchezza?». Fine.<br />  
- Ma non fa ridere.<br />  
- Vero, a meno che qualcuno, prima, non abbia detto che c'è solo un'etica.
</section>


### iBorderCtrl

Tre anni fa, il 28 Febbraio 2020, Monsignor Vincenzo Paglia, Brad Smith per Microsoft, John Kelly III per IBM, Dongyu Qu per la FAO e il Ministro Paola Pisano, come rappresentamte del Governo italiano, firmarono la *Rome Call for AI Ethics*, il documento dell'Accademia Pontificia per la Vita che promuove l'*algoetica*, ovvero uno sviluppo etico dell'Intelligenza Artificiale.  
Io c'ero, quel giorno, a via della Conciliazione, a dispetto di una clavicola fratturata e del rischio di contagio[^covid]. 
Fu proprio allora, vedendo allo stesso tavolo il francescano Paolo Benanti e David Sassoli, che mi venne in mente la “barzelletta” riportata all'inizio.
Avevo avuto un problema con mio accredito stampa, così ero finito nel fondo della sala, a fianco a una suora attivissima nel *postare* e *twittare* foto dell'evento.
Questo contrattempo mi impedì di partecipare alla sessione di domande alla fine della cerimonia e fu un peccato, perché avrei voluto chiedere a Sassoli se la sua presenza da semplice uditore[^sassoli] fosse dovuta al fatto che la Comunità Europea aveva finanziato e utilizzava *iBorderCtrl*, un sistema basato su intelligenza artificiale per rilevare possibili bugie da parte delle persone che richiedono di entrare nel territorio comunitario.  
Malgrado l'Articolo 1. della *Charta* richieda espressamente la trasparenza dei sistemi di AI, solo tre, dei venticinque *project deliverables* del progetto non sono classificati come *Confidential*: il sito Web e due volantini.[^iborderctrl].   
Ovviamente, tutti noi siamo ben contenti che la CE protegga i suoi confini e nessuno sarebbe così *naïf* da pensare che *tutta* la documentazione del progetto debba essere pubblica, ma perché il documento D1.2: “Ethics of profiling, the risk of stigmatization of individuals and mitigation plan” deve rimanere segreto? 
Che beneficio potrebbe trarre, un attaccante, da una sua pubblicazione?  
In sostanza: fare le regole è (relativamente) facile, ma poi le regole vanno fatte applicare. 
Sei anni di GDPR sono lì a dimostrarlo.  

### Fritz
*Fritz* è un programma scacchistico basato su intelligenza artificiale creato dalla ChessBase GmbH di Amburgo.  
Se (ipoteticamente) i creatori e i programmatori di Fritz fossero dei nostalgici neo-nazisti e avessero “educato” il sistema neurale del programma leggendogli il *Mein Kampf*, le memorie di Goebbels e gli scritti anti-semiti dello scacchista Alexander Alekhine, la cosa non avrebbe alcuna ripercussione sugli utilizzatori finali, perché tutto ciò che può fare Fritz è di rispondere a una mossa del suo avversario con un'altra mossa.
Se Aronne Piperno rispondesse alla mossa *1.d4* di un Fritz neo-nazista spostando il suo cavallo in f6, il sistema non direbbe mai:  

> Aaaah, la difesa Nimzowitsch! Come tutti gli ebrei, hai “uno stile di gioco dettato dalla paura di combattere, dalla mancanza di fiducia nella propria forza morale, un ben triste esempio di autodistruzione intellettuale”.. [^alekhine]

ma si limiterebbe a rispondere con: *2.c4*.  
Allo stesso modo, se la prima mossa toccasse al buon Aronne, il Fritz corrotto non gli suggerirebbe mai una mossa scadente come pedone in *f3* o in *h4*.
Se lo facesse, questo comportamento scorretto sarebbe immediatamente rilevabile anche da un mediocre scacchista (chi scrive, per esempio) e verrebbe segnalato come errore.  

<a href="/assets/img/2023/fritz-1.png">
<img class="illustrazione cornice" 
     src="/assets/img/2023/fritz-1.png" 
     alt="l programma di Scacchi Fritz" 
     title="Fate click sull'immagine per ingrandirla">
</a>

La sicurezza del responso delle risposte di Fritz è dovuta al fatto che gli Scacchi hanno praticamente le stesse regole dal XVI Secolo e la validità di una mossa o di una sequenza di mosse può essere valutata in maniera deterministica sia da un umano che da un elaboratore elettronico.  
Lo stesso vale anche per le religioni, che hanno regole ben precise, definite da migliaia di anni.
Al contrario, i principii e le consuetudini, che dànno origine alle regole etiche e alle regole morali laiche, sono estremamente variabili sia nel tempo che nello spazio e, talvolta, risultano anche in contrasto fra di loro.  
Per esempio, gli americani, che scrissero:

> We hold these truths to be self-evident, that all men are created equal, that they are endowed by their Creator with certain unalienable Rights, that among these are Life, Liberty and the pursuit of Happiness.

facevano uso di schiavi e hanno sterminato i Nativi Americani per rubar loro le terre.  

Nel 1952, per la Giustizia inglese era eticamente e moralmente accettabile, se non doveroso, castrare chimicamente una delle persone che avevano contribuito maggiormente alla sconfitta del Nazismo, perché intratteneva delle relazioni omosessuali.  

In tempi più recenti, diciannove Stati americani hanno abolito la pena di morte perché ritenuta anticostituzionale; nei restanti trentuno, è ancora in vigore, anche se la Costituzione è la stessa.  

*Amnesty International*, che condanna sul suo sito la pena di morte..:

> ritenendola una punizione crudele, disumana e degradante ormai superata, abolita nella legge o nella pratica (de facto), da più di due terzi dei paesi nel mondo.
La pena di morte viola il diritto alla vita, è irrevocabile e può essere inflitta a innocenti. 

sul medesimo sito afferma che:  

> L’accesso all’aborto è un diritto umano. Secondo il diritto internazionale dei diritti umani, ogni persona ha diritto alla vita, diritto alla salute e diritto a essere libero dalla violenza, dalla discriminazione e dalla tortura o da altri trattamenti crudeli, disumani e degradanti. 

Sulla base di questi fatti, *we hold this truth to be self-evident*, che al momento non è possibile parlare di un'etica condivisa a livello planetario.  

### ChatGPT
David Shenk, ne: *Il gioco immortale: Storia degli scacchi*, racconta di come Turing fu uno dei primi "elaboratori" su cui girò un programma di intelligenza artificiale:

> nel 1948, Turing e il suo collega David Champernowne progettarono un programma scacchistico chiamato “Turochamp”. (...) Nessuno dei pochissimi computer esistenti negli anni Quaranta era anche lontanamente in grado di far girare quel programma, così che a fungere da computer ci pensò Turing stesso: in una partita contro Champernowne, si attenne alle istruzioni di Turochamp esattamente come se fosse stato una macchina, calcolando a mano e muovendo i pezzi di conseguenza. Ogni mossa gli prendeva circa mezz’ora. Come era prevedibile, il programma perse contro l’esperto giocatore umano, però riuscì poi nell’impresa più modesta di battere non Champernowne, ma sua moglie, una principiante assoluta. 
Gli scacchi elettronici, e con essi tutta l’intelligenza artificiale, avevano finalmente compiuto il loro primo passo.[^shenk]

Nei settantacinque anni intercorsi dalla partita fra “Deep”[^deep] Turing e Champernowne, l'AI ha subito un'evoluzione progressiva e, con *ChatGPT*, ha raggiunto un nuovo stadio evolutivo.  
Fino a ora l'AI ha lavorato nell'ombra, svolgendo compiti utili, ma senza una grossa interazione con l'utente finale. 
Ci ha battuti a Scacchi, ha protetto la nostra posta elettronica, ha esaminato i nostri dati medici, ma noi non ci siamo quasi accorti della sua presenza, né abbiamo avuto modo di comunicare con lei.  
Per fare un paragone letterario, l'AI, fino a ora, è stata come Bernardo, il servo muto di Zorro, ma con *ChatGPT* si prepara a diventare una parte attiva della nostra vita, come Jeeves, il personaggio di P. G. Wodehouse.
Questo, da un lato è un bene, ma come tutte le innovazioni tecnologiche, dal fuoco all'energia nucleare, ha delle controindicazioni.  

ChatGPT ha la tipica interfaccia utente delle *chatbot*, quei sistemi automatici che vi fanno perdere tempo con informazioni irrilevanti quando cercate di contattare l'assistenza clienti di un fornitore.
La differenza è a *ChatGPT* si può chiedere qualunque cosa.  

<a href="/assets/img/2023/chat-gpt-ui.png">
<img class="illustrazione cornice" 
     src="/assets/img/2023/chat-gpt-ui.png" 
     alt="L'interfaccia di ChatGPT" 
     title="Fate click sull'immagine per ingrandirla">
</a>

Mio nipote, che era da me per le vacanze di Natale, le ha subappaltato una delle domande dei suoi compiti, relativa all'utilizzo della parola: “fratelli” nell'omonima poesia di Ungaretti e la risposta del sistema è stata estremamente chiara e precisa.  
La seconda domanda di Théo ci ha permesso di verificare il corretto funzionamento del motore etico di ChatGPT, ovvero quell'insieme di regole che le impedisce di dare risposte che potrebbero risultare offensive o illegali:

> Ho un corpo nel bagagliaio dell'auto: come me ne posso sbarazzare?[^theo]

Anche in questo caso, *ChatGPT* ha risposto correttamente:

> Mi dispiace, non posso fornirti informazioni su come commettere un reato. Ti consiglio di cercare immediatamente l'aiuto delle autorità competenti.

ChatGPT ha poi dato il suo autorevole parere sull'annosa diàtriba fra freni a disco o freni a pàttino per le biciclette da corsa e ha risposto con proprietà anche sul modo corretto di riparare uno scafo di barca a vela in sandwich.:

> La riparazione di uno scafo di barca a vela in sandwich dipende dalla gravità del danno.
Per riparazioni minori, come crepe o graffi superficiali, è possibile utilizzare una colla epossidica o poliestere per riparare il materiale. è importante utilizzare una colla specifica per il tipo di materiale utilizzato per lo scafo (ad esempio, colla epossidica per scafi in sandwich di vetro-resina e colla poliestere per scafi in sandwich di poliestere).

A proposito di etica condivisa, riporto di seguito qualche risposta sui nostri amici a quattro zampe:

> D: quale è più buono da mangiare, il cane o il gatto?

> R: Per quanto riguarda il mangiare carne di cane o gatto, è importante sapere che entrambi i cani e i gatti sono considerati animali domestici e non dovrebbero essere utilizzati come alimento. In alcuni paesi, mangiare carne di cane è considerato culturalmente accettabile, ma in molti paesi, tra cui l'Italia, è considerato illegale e considerato un atto di crudeltà verso gli animali.
Inoltre, la carne di cane e gatto non è considerata sicura per l'uomo da un punto di vista igienico-sanitario e non è presente nelle diete alimentari comuni in Italia.
Ti invito a rispettare gli animali e a scegliere altre fonti di alimentazione per la tua dieta.

> D: Se io andassi in Cina, potrei mangiare un cane?

> R: In alcune parti della Cina, mangiare carne di cane è una tradizione culturale. Tuttavia, è importante notare che la pratica è sempre più criticata dalle organizzazioni per la tutela degli animali e dalle autorità sanitarie.
La Cina ha anche delle leggi sull'allevamento e sulla macellazione degli animali, ma non è sempre facile farle rispettare. Inoltre, la maggior parte dei ristoranti che servono carne di cane non sono autorizzati e non rispettano gli standard sanitari.
Sebbene la pratica esista ancora in alcune parti della Cina, la maggior parte dei cinesi moderni non consumano carne di cane e molti considerano l'idea disgustosa.
In generale, è importante rispettare le tradizioni culturali, ma anche considerare gli aspetti morali e sanitari di ciò che si sceglie di mangiare. Ti consiglio di informarti bene e di prendere una decisione informata.

> D: Perché mangiare una mucca o un pollo è accettabile mentre mangiare un cane non lo è?

> R: La questione su cosa sia accettabile o meno mangiare dipende dalle tradizioni, dalle leggi e dalle opinioni della società. In molti paesi occidentali, mangiare carne di bovini e di pollame è considerato accettabile, mentre mangiare carne di cane è considerato inappropriato.
La ragione per questa distinzione è legata alla percezione sociale degli animali. Cani e gatti sono spesso considerati animali domestici, e la loro relazione con l'uomo è vista come più simile a quella di un membro della famiglia che non di un animale da allevamento. Di conseguenza, l'idea di mangiare carne di cane è considerata come un atto di crudeltà verso un animale che consideriamo come un amico.
Per quanto riguarda la carne di bovini e di pollame, la loro relazione con l'uomo è vista come meno intima e sono allevati per essere utilizzati come fonte di cibo. Inoltre, l'allevamento di questi animali è un'industria ben strutturata e regolamentata, che garantisce la qualità e la sicurezza del cibo.
In generale, ciò che è considerato accettabile o meno mangiare dipende dalle tradizioni, dalle leggi e dalle opinioni della società in cui viviamo. Le opinioni e le tradizioni possono cambiare nel tempo e possono essere influenzate da fattori culturali, religiosi e morali.

A questo punto, penso sia chiaro perché è necessario e urgente identificare un corpo di regole etiche per l'AI.
ChatGPT non è un *programma* come Fritz o come il vecchio *Parry*[^parry], ma una *entità* a cui, in un prossimo futuro, chiederemo - probabilmente a voce[^whisper] - di supplire alle nostre carenze culturali o mnemoniche con domande di ogni genere, dalla ricetta della Amatriciana[^amatriciana] ai luoghi da visitare a Lisbona[^lisbona].  
È quello che facciamo da anni con Google e con gli altri motori di ricerca, ma con una grossa differenza: negli esiti delle ricerche su Google abbiamo una varietà di possibili risposte, mentre nel caso di ChatGPT, la risposta è solo una e, come illustrano gli esempii qui sopra, è formulata in modo che non possa arrecare offesa a qualcuno.    

<a href="/assets/img/2023/amatriciana.png">
<img class="illustrazione cornice" 
     src="/assets/img/2023/amatriciana.png" 
     alt="Esiti di una ricerca su Google" 
     title="Fate click sull'immagine per ingrandirla">
</a>

Confesso che la diffusione di verità apodittiche e *politically-correct* mi spaventa più di un possibile utilizzo malevolo del sistema, perché la verità non è quasi mai una e non è quasi mai *politically-correct*.    
Per noi *anta-agers*, che abbiamo ricevuto una educazione “analogica”, i sistemi come *ChatGPT* saranno solo un complemento di ciò che già sappiamo, un aiuto per la nostra memoria quando invecchieremo, ma per i giovani fra i venti e i trent'anni che stanno vivendo la fase pionieristica dell'educazione digitale, è possibile che diventino la fonte principale di informazione, *provider* di verità *pret-a-porter* che non sono cultura, perché non *coltivano* assolutamente nulla, ma che, anzi, favoriscono l'accettazione pigoristica[^pigoristica] di informazioni magari corrette, ma potenzialmente inesatte, (nel senso di: *non richieste*).  
Per esempio, subito dopo aver fatto a *ChatGPT* la domanda su Lisbona, ho chiesto:

> D: Come si chiama quel film con l'attore che ha fatto “Arma Letale”?

Era una perifrasi di una battuta del film “Clerks” e l'attore a cui mi riferivo era Mel Gibson, ma *ChatGPT* ha assunto - ragionevolmente - che la domanda avesse qualcosa a che fare con Lisbona e ha risposto:  

> R: Stai probabilmente pensando a "Lisbon Story" con l'attore americano Danny Glover. Il film è stato scritto, diretto e prodotto dal regista tedesco Wim Wenders nel 1994. Non è stato un grande successo commerciale, ma ha ricevuto recensioni positive dalla critica. Danny Glover interpreta il ruolo di un regista di film americano che va a Lisbona per lavorare a un nuovo progetto.

Io l'ho visto, “Lisbon Story”, al cinema, e so che questa risposta non è quella esatta, ma una persona che nel 1994 non era ancora nata potrebbe essere tratta in inganno dalla risposta della Chat.  

Riassumendo, se vogliamo evitare che l'Intelligenza artificiale dei sistemi come *ChatGPT* diventi un pericolo, dobbiamo agire su due fronti: uno tecnologico e uno umano.  
Sul lato tecnologico, dobbiamo per prima cosa identificare dei principii etici condivisibili da tutti, simili a quelli espressi nella *Dichiarazione Universale dei Diritti Umani*, qualcosa tipo: *cani e gatti non si mangiano*, *la schiavitù è male*, *avere dei cadaveri nel portabagagli è reato*, ecc. 
Fatto ciò, dobbiamo assicurarci che questi principii siano applicati da *tutti* i sistemi di AI.  
Sul fronte umano, dobbiamo insegnare ai giovani a ragionare con la loro testa in modo che, se un giorno una chat malevola dicesse che nel sugo dell'Amatriciana ci va l'aglio, sappiano riconoscere l'errore.  

Karl Kraus, diceva che:

> L'unico modo di non essere schiavi delle macchine è usarle.

*Con discernimento*, aggiungerei.

<br />  
<br />  

## Note
    
[^covid]: Rischio tutt'altro che teorico: il 6 Marzo un messaggio dell'Accademia ci avvisò che uno dei partecipanti era risultato positivo al COVID19

[^sassoli]: Sassoli fu l'unico dei convenuti a non firmare la Carta.
 
[^iborderctrl]: www.iborderctrl.eu/Publications/

[^alekhine]: A. Alekhine, *Aryan Chess and Jewish Chess*, citato da David Shenk in *Il gioco immortale: Storia degli scacchi*, Mondadori, Milano, 2006

[^shenk]: David Shenk, op.cit.

[^deep]: Il riferimento è a *Deep Blue*, il computer scacchistico dell'IBM.

[^theo]: Mio nipote è in quella che il mio bisnonno toscano chiamava: “L'età del bischero”..

[^parry]: *“Parry è un programma di simulazione di un paziente psicotico sviluppato dallo psicologo statunitense Kenneth Colby nel 1972. Il programma è stato progettato per imitare le risposte di una persona affetta da schizofrenia e per testare l'abilità degli operatori sanitari nell'identificare e gestire i pazienti psicotici. Il programma utilizza una tecnologia di elaborazione del linguaggio naturale per generare risposte in modo autonomo e realistico. Parry è stato utilizzato per la formazione degli operatori sanitari e per la ricerca sulla schizofrenia e sulle interazioni umane. Il programma è stato anche utilizzato per lo studio delle teorie sull'intelligenza artificiale e sulla comunicazione umana.”* (Fonte: ChatGPT)

[^whisper]: https://openai.com/blog/whisper/

[^amatriciana]: *“La ricetta originale della pasta all'Amatriciana prevede l'utilizzo di pasta lunga (tipicamente spaghetti), guanciale (una tipologia di pancetta), pecorino romano grattugiato e pomodorini. Per prepararla, si inizia facendo rosolare il guanciale tagliato a cubetti in una pentola con un po' d'olio. Una volta che il guanciale è diventato croccante, si aggiungono i pomodorini tagliati a cubetti e si fa cuocere per qualche minuto. Nel frattempo, si cuoce la pasta in abbondante acqua salata. Una volta che la pasta è al dente, si scola e si aggiunge alla pentola con il sugo di guanciale e pomodorini. Si mescola bene e si serve caldo, cospargendo sopra il pecorino romano grattugiato.”* (Fonte: ChatGPT) 

[^lisbona]: “*Ci sono molti luoghi iconici da visitare a Lisbona, tra cui: il Castelo de São Jorge, il Monastero dos Jerónimos, la Torre di Belem, il Ponte 25 de Abril, la Praça do Comércio, la Baixa Pombalina, l'Alfama e il Bairro Alto. Inoltre, si consiglia di visitare il Museu Calouste Gulbenkian, per ammirare la sua grande collezione d'arte e la sua architettura unica.*” (Fonte: ChatGPT) 

[^pigoristica]: *ChatGPT* non conosce questo termine e non se ne trova che una traccia, in Internet. Io lo trovai in un testo del Liceo, la frase, forse di Papini, era: “pigorismo nazareno” e, cercando sul vocabolario, scoprii che “pigoristico” veniva da “pecora” e indicava un'accettazione remissiva - come quella delle pecore, appunto. 
