---
category:   "sicurezza"
excerpt:    "I siti di incontri, sono un bene o un male? Sì"
h1:         "L'amore al tempo di Meetic - seconda parte"
h2:         "Prevenire è meglio di stalking."
layout:     post
image:      "/assets/img/2015/logo-meetic-motto.jpg"
thumbnail:  "/assets/img/2015/logo-meetic-motto.jpg"
img-alt:    "Il logo di Meetic"
permalink:  "/sicurezza/amore-al-tempo-di-meetic-seconda-parte.html" 
tags:       [sicurezza, Meetic, dating online]
title:      "L'amore al tempo di Meetic - seconda parte"
---

Nella <a href="/sicurezza/amore-al-tempo-di-meetic-prima-parte.html" class="xref">prima parte</a> di questo articolo, ho definito alcune regole per l'utilizzo sicuro dei siti di incontri:

1. non usare uno pseudonimo che sia l'unione del vostro nome e del vostro cognome;

2. non utilizzare, nei siti in cui comparite sotto pseudonimo, le stesse foto che avete pubblicato altrove; 

3. differenziare le amicizie sui social-network e limitare la visibilità delle immagini e delle informazioni pubblicate;

4. separare il lavoro dalla vita privata;

5. ricordarsi che Google trova anche i numeri di telefono.

Vediamo adesso come mettere in pratica queste norme di comportamento.

### Non usare uno pseudonimo che sia l'unione del vostro nome e del vostro cognome
Non offenderò la vostra intelligenza spiegando questo concetto.
Ricordatevi, però, che, se avete un nome particolare, è altrettanto sconsigliabile utilizzarlo come pseudonimo, anche se privo del cognome, perché potrebbe essere utilizzato in una ricerca insieme ad altri vostri dati personali, identificandovi.
Per esempio, se vi chiamate Zenaide, fate l'archeologa ed avete pubblicato un saggio o un articolo sui Sassi di Matera, una ricerca ben fatta potrebbe portare al vostro saggio e quindi a voi.
È sconsigliabile anche utilizzare su un sito di incontri lo stesso pseudonimo che utilizzate sui social-network, perché potrebbe (anzi: potrà) aiutare un male-intenzionato a reperire informazioni su di voi.  

### Non pubblicare, nei siti in cui comparite sotto pseudonimo, le stesse foto che avete pubblicato altrove
A Maria piace ballare e nel suo profilo Meetic ha inserito una foto che compare anche nella pagina Facebook o sul sito Web della sua scuola di Tango. 
Un suo "ammiratore" inserisce la foto nella maschera di ricerca immagini di Google, aggiunge la scritta: "tango" e pochi secondi dopo sa dove trovare Maria tutti i mercoledì sera, fino a tarda notte.
Se non volete che questo accada, nei siti in cui comparite sotto pseudonimo non utilizzate le stesse foto che compaiono nei siti dove siete registrata con il vostro vero nome.
Se poi volete unire l'utile al sicuro, visto che le foto sono il primo punto di contatto fra voi e il vostro potenziale Principe Azzurro, fatevi &mdash; o, ancora meglio, fatevi fare da un bravo fotografo &mdash; delle foto che userete solo per i siti di incontri a cui siete iscritte: ne gioveranno sia la vostra immagine che la vostra tranquillità.

### Differenziare le amicizie sui social-network e limitare la visibilità delle immagini e delle informazioni pubblicate

Ci molti modi in cui una foto può tradirvi:

- se nella foto si vede la targa della vostra auto o del vostro motorino, potrebbe essere molto facile risalire a voi;

- sapere che lavorate alle Poste non è pericoloso, ma farsi una foto davanti all'agenzia in cui lavorate e pubblicarla in Rete può consentire a uno spasimante indesiderato di rintracciarvi senza grossa fatica;

- siete orgogliose dei vostri figli: fate bene, sono meravigliosi, ma pubblicare le loro foto su Internet può essere estremamente pericoloso. Non lo fate.

Stesso discorso vale per qualsiasi informazione riguardante la vostra vita. 
Il messaggio:

> Domani parto per le vacanze, ci rivediamo fra quindici giorni 

potrebbe essere accolto con invidia dai vostri amici, ma con gioia da chi vuole svaligiarvi casa.

Se poi sentite l'irrefrenabile bisogno di sfoggiare il vostro motorino nuovo, il lussuoso albergo in cui villeggiate o i riccioli di vostro figlio, abbiate cura di farlo solo con chi conoscete.
Tutti i principali social-network permettono di suddividere le "amicizie" in gruppi e di stabilire cosa possano vedere o non vedere gli appartenenti a ciascun gruppo. 
È un'operazione facile e rapidissima che vi permette di pubblicare informazioni personali evitando che possano essere viste dagli estranei o dalle persone di cui non desiderate l'attenzione.

Per vivere tranquilli basta definire tre gruppi e tre livelli di visibilità:

| gruppo | chi inserire | cosa mostrare |
|---|---|---
| **amici** | parenti, amicizie reali e chiunque si conosca personalmente | tutto ciò che pubblicate, tanto, lo sanno chi siete..
| **conoscenze** | colleghi, amicizie virtuali, persone a cui non volete far sapere tutti i fatti vostri | notizie o foto che non vi riguardano personalmente, p.es. foto di tramonti, battute umoristiche, foto di gattini tenerissimi ecc.
| **pubblico** | il resto dell'Umanità | niente, non sono fatti loro.

### Separare il lavoro dalla vita privata

Se utilizzate i social-network per pubblicizzare la vostra attività, siete obbligate a diffondere notizie su di voi e, anzi, dovete cercare di fare in modo che raggiungano il maggior numero di persone possibile.
È quindi necessario, per non contravvenire alla regola precedente, separare le informazioni relative alla vostra attività lavorativa da quelle riguardanti la vostra vita privata.
Anche questo non è difficile e non richiede - almeno su Facebook - la registrazione di una nuova utenza, ma solo la creazione di una pagina dedicata all'attività che intendete pubblicizzare.  
Per fare un esempio, immaginiamo che la signora Maria, quella a cui piace ballare, abbia una piccola attività artigianale o una libreria o qualsiasi altra attività che la ponga necessariamente in contatto con il pubblico.
Se Maria inserisce nel suo profilo Facebook anche informazioni relative alla sua attività, il solito ammiratore ostinato potrebbe decidere di andarla a trovare sul luogo di lavoro o, peggio, spiarla quando esce per tornare a casa. 
Se invece Maria inserisce le informazioni relative al suo lavoro in una pagina separata, non riconducibile al suo profilo personale, il suo spasimante dovrà rassegnarsi a non vederla più.  
Questa protezione vale anche in senso inverso, ovviamente, perché impedisce ai clienti di Maria di risalire alle sue informazioni personali..

###  Ricordarsi che Google trova anche i numeri di telefono
I numeri di telefono hanno una struttura ben definita e sono molto più facili da ricercare di informazioni generiche come, per esempio: 

> Zenaide archeologa Matera

Per verificarlo, cercate su Google la stringa: 

> 06 36851 

e guardate quanto ci mette a scoprire che si tratta del telefono dello Stadio Olimpico. 
State pur certe che ci metterà lo stesso tempo a trovare il vostro numero di telefono, se lo avete pubblicato in un annuncio per la vendita di un'auto su E-Bay, sul sito Web della vostra attività, o nell'accorato appello per l'adozione di un cane randagio.
Tenetene conto, prima di cominciare a scambiarvi SMS con qualcuno che non conoscete.

Un'ultima raccomandazione, prima di chiudere:

### NON SOTTOVALUTATE IL PROBLEMA

A Gennaio del 2015, Meetic aveva 42 milioni di iscritti, di cui 2.800.0002 solo in Italia . 
Di questi due milioni ottocentomila iscritti, il 59% sono uomini, per un totale di 1.428.000 individui.  
Se anche solo uno su mille di questi signori fosse un malandrino, voi dovreste guardarvi da 1.428 potenziali molestatori.  
La redazione del sito svolge un accurato controllo delle informazioni immesse dagli utenti e verifica che i testi non contengano informazioni che possano identificare l'utente, ma quando il rapporto con l'interlocutore si trasferisce ad altri mezzi di comunicazione come la posta elettronica o il telefono, i vostri angeli custodi telematici non possono fare più nulla per voi e potete far conto solo sulla vostra prudenza e sul vostro buon senso.  
Come dice il motto del sito: "Incontrarsi è solo l'inizio" e Meetic, su questo, può aiutarvi, mettendovi in contatto con decine di migliaia di possibili candidati, ma siete solo voi che potete far sì che la vostra favola, appena iniziata, abbia un lieto fine.
