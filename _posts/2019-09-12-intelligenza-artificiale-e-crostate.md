---
category:   "informatica"
excerpt:    "Considerazioni sul nostro futuro digitale, dopo la lettura del numero 85 della rivista Aspenia"
h1:         "Intelligenza artificiale e crostate"
h2:         "Considerazioni sul nostro futuro digitale, dopo la lettura del numero 85 della rivista Aspenia"
image:      "/assets/img/2019/ai-macht-frei.png"
img-alt:    "AI macht frei"
layout:     post
permalink:  "/informatica/intelligenza-artificiale-e-crostate.html" 
tags:       [informatica, intelligenza artificiale]
thumbnail:  "/assets/img/2019/ai-macht-frei.png"
title:      "Intelligenza artificiale e crostate"
---

<section class="citazione">
A Just Machine to make big decisions<br/>
Programmed by fellows with compassion and vision<br/>
We'll be clean when their work is done<br/>
We'll be eternally free yes and eternally young<br/>
Donald Fagen - <i>I.G.Y.</i>
</section>

<p style="color:#999;font-size:0.9rem">
I codici QR si riferiscono ai documenti o ai siti citati nel testo.
</p>

## Cosa c'entrano le crostate
Nel suo libro *L'ordine del Tempo*, il fisico Carlo Rovelli afferma che il tempo scorre più lentamente a livello del mare che in cima a una montagna a causa della massa della Terra, che influenza lo spazio-tempo.
Non è vero: ciò che fa scorrere più lentamente il tempo è la presenza delle mamme anziane, più numerose a livello del mare che sulla vetta delle montagne.
Prova ne sia il fatto che, da quando mia madre è venuta a trovarmi, le ore, che prima fuggivano via come avannotti inseguiti da una spigola, si sono dilatate e sembrano non finire mai.
<img src="/assets/img/2019/qr-aspenia.png" class="qr primo">
Stando così le cose, ho deciso di approfittare di questa espansione temporale per leggere gli articoli sull'Intelligenza Artificiale pubblicati nel numero 85 della rivista *Aspenia*<a href="#aspenia" class="nota" name="fn-1">1</a>.
È stata una buona idea, perché sono tutti articoli molto interessanti, che descrivono da punti di vista differenti i molteplici aspetti di questa "nuova" tecnologia e che mi hanno permesso di chiarire alcuni dubbi che avevo al riguardo.
Ciò non ostante, credo che si possa applicare all'AI un detto che lessi originariamente riferito agli economisti:

> Se vuoi avere tre opinioni diverse su un problema di Intelligenza Artificiale, chiedi a due esperti di Intelligenza Artificiale.

Trovo che questa generale indeterminatezza sul futuro dell'AI sia normale e comprensibile: è un settore talmente nuovo e pervasivo che è impossibile poter affermare con buona certezza quali sviluppi potrà avere in futuro; si possono fare solo delle ipotesi e io vorrei, qui, fare la mia, partendo dal presupposto che la buona Intelligenza Artificiale sia assimilabile a una crostata.
In un *post* sulla sua pagina Facebook, il professor Luciano Floridi ha raccontato di come ogni anno, il giorno del loro anniversario, prepari una crostata per sua moglie.
La lettura di questo toccante squarcio sulla vita affettiva e sulle abitudini culinarie di uno dei massimi esperti mondiali di Intelligenza Artificiale ha evidentemente generato dei collegamenti sinaptici fra l'area del mio cervello che si occupa di informatica e quella, molto più estesa, che si occupa di crostate e mi ha portato a formulare un modello a crostata per la [buona] Intelligenza Artificiale.

<img class="illustrazione cornice" src="{{ page.image }}" >

Come tutti sanno, una crostata è scomponibile in quattro parti distinte:

- **uno strato dolce**, costituito da marmellata o da composta;
- **una base di pastafrolla**, che sorregge lo strato dolce;
- **un bordo** che delimita lo strato dolce impedendogli di cadere dalla base;
- **un reticolo di pastafrolla** sovrapposto allo strato dolce, che lo suddivide in riquadri dandogli ordine e facendo sì che la densità dello strato si mantenga costante in ogni punto.

Trasportando questo modello nei termini dell'IA, otteniamo:

- **uno strato dolce**, costituito dalle ricadute positive dell'IA;
- **una base tecnologica**, che sorregge lo strato dolce;
- **un bordo etico** che ne definisce i confini di applicazione;
- **un reticolo di regole**, che, dipartendosi dal bordo, dà ordine ed evita la formazione di squilibrii nello strato dolce.

In quest'ottica, la domanda da cui dipende la qualità del nostro futuro è molto semplice:

> Siamo in grado di preparare una buona crostata?

Vediamo.

## Lo strato dolce
Io non so fare le crostate come il professor Floridi, ma sono un apprezzato produttore di marmellate e posso dire con buona certezza che la qualità di una marmellata dipende quasi del tutto dalla qualità degli agrumi che si utilizzano.
Per fare una buona marmellata è fondamentale utillizzare degli agrumi che non siano stati in alcun modo trattati con sostanze chimiche, perché queste si trasferirebbero inevitabilmente nel prodotto finale.
Similmente, la qualità dell'*output* di un sistema di IA dipende in buona misura dalla qualità dei dati con cui è stato “educato” il sistema, se questo, come un giocatore di carte, deve basare le proprie valutazioni sugli eventi del passato.
<img src="/assets/img/2019/qr-floridi.png" class="qr">
Fabrizio Floridi (padre di Luciano), nel suo manuale: 
*Prima Fase di Apertura degli Scacchi: Compendio per un dilettante autodidatta in un mondo digitale*<a href="#floridi-f" class="nota" name="fn-2">2</a>,
fa un'acuta distinzione fra il modo di decidere di un giocatore di scacchi e un giocatore di carte:

> Negli Scacchi la materia su cui la capacità logica si esercita è la rappresentazione delle future posizioni, all’opposto del gioco delle carte, dove prevale sulla memoria del pregresso.

Lo stessa distinzione vale per i sistemi di AI: il navigatore di una *smart-car* non ha alcun bisogno di sapere quante auto ci fossero a un determinato incrocio il giorno prima e accelera o frena in base alla situazione corrente; al contrario, un sistema di sostegno alle decisioni analizza un determinato caso facendo riferimento a ciò che è avvenuto in casi simili in passato.
Se le informazioni di cui dispone sono in qualche modo alterate, lo sarà anche il giudizio finale del sistema.
<img src="/assets/img/2019/qr-propublica.png" class="qr">
Giuseppe Vaciago, nel suo articolo “Cyber cop contro criminal bot”, a pagina 123 di *Aspenia*, racconta di come l'associazione ProPublica abbia dimostrato che il sistema COMPAS (*Correctional Offender Management Profiling for Alternative Sanctions*), che dovrebbe valutare la probabilità che un imputato possa rivelarsi recidivo, tenda, a parità di condizioni, a ritenere più probabile la recidività di un imputato di colore rispetto a uno di altre etnie.<a href="#ProPublica" class="nota" name="fn-3">3</a> <img src="/assets/img/2019/qr-allenai.png" class="qr">
Problemi simili si verificano anche nel settore medicale e potete sperimentare personalmente una forma sintetica di sciovinismo maschilista accedendo al sito dell'Allen Institute for Artificial Intelligence<a href="#allenai" class="nota" name="fn-4">4</a> e inserendo nel loro generatore automatico di frasi le parole:

> A woman is always

selezionando poi le parole che hanno il maggior punteggio percentuale:

- *looking* (16,4%)
- *for* (67,1%)
- *a* (29,3%)
- *way* (12,7%)
- *to* (76,8%)
- *make* (9,9%)
- *money* (21,0%)

La scelta romantica:

- *make* (9,9%)
- *her* (14,4%)
- *husband* (20,9%)
- *happy* (23,5%)

è al secondo posto, con circa il 7% di probabilità in meno.
La dipendenza dati/esito mi sembra essere l'unica certezza che abbiamo, su quelle che potrebbero essere le ricadute positive o negative dell'AI; tutto il resto sono ipotesi e per ciascuna di esse si possono trovare, in completa buona fede, tanti motivi a favore che a sfavore.
<img src="/assets/img/2019/qr-pew.png" class="qr">
Domenico De Masi, nel suo articolo “Lavorare meno per vivere da umani”, riporta gli esiti di un recente studio del Pew Research Center di Washington<a href="#pew" class="nota" name="fn-5">5</a> sul futuro dell'AI: il 48% degli intervistati si è detto preoccupato dai possibili effetti dela robotizzazione sull'occupazione; il restante 52% non lo è, per motivi altrettanto validi.
Per quanto riguarda l'occupazione, io mi schiero con il 52% degli ottimisti, ma con una precisazione che dettaglierò nel prossimo paragrafo; su quello che sarà invece il bilancio complessivo della diffusione dell'AI, ho una posizione simile a quella di Moana Pozzi, che una volta disse:

> Non credo troppo nel progresso, perché aumentando le scoperte aumentano anche i problemi. E dunque siamo sempre allo stesso punto, no?<a href="#moana" class="nota" name="fn-6">6</a>


## La base
Ne sono passati, di byte sotto i router, da quando una delle Due Roberte, le mie graziose e simbiotiche colleghe che, nel 1990, studiavano le reti neurali al reparto di Ricerca e Sviluppo di Bonifica, gridò da una parte all'altra dell'*open-space* in cui lavoravamo:

> Gli ho insegnato la "D"!

Gli algoritmi amatoriali che utilizzavamo allora si sono evoluti &mdash; così come l'*hardware*, del resto &mdash; e oggi disponiamo di tutto ciò che ci occorre per realizzare dei sistemi di AI potenti e affidabili.
Probabilmente ci vorrà ancora qualche anno prima che si riesca a produrre una *singolarità*<a href="#singolarità" class="nota" name="fn-7">7</a>, ma questo è un bene.
L'unica componente del sistema che non si è evoluta in questi trent'anni sono i tecnici.
Mentre Le Roberte &mdash; entrambe laureate in statistica &mdash; insegnavano a leggere alla loro rete neurale, io, che provenivo da tutto un altro settore, studiavo il linguaggio C sul manuale di Kernigan e Ritchie e sbattevo la testa sull'algebra relazionale di Chen, indirizzato e consigliato da due colleghi più esperti che mi hanno trasmesso la loro esperienza, evitandomi il fastidio di ripetere i loro errori.
Oggi, però, tutto questo non c'è più: l'affiancamento, la formazione, le revisioni di codice sui tabulati pieghevoli delle stampanti a 136 colonne con i bordi bucherellati che li potevi staccare e farci i rotolini mentre il tuo mentore ti spiegava perché eri un idiota, sono stati spazzati via proprio dall'incremento dell'occupazione che è seguito alle tre grandi rivoluzioni tecnologiche degli ultimi anni: il boom di Internet dei primi anni 2000, il fenomeno *mobile* e infine la sicurezza.
In settori come l'agricoltura, l'edilizia o il commercio, un rapido incremento dell'occupazione è sicuramente un bene, ma in settori altamente specialistici come l'informatica è un male, perché quando l'aumento della domanda non può più essere soddisfatto con personale esperto si è costretti a utilizzare persone con competenze sempre più eterogenee, formandole alla meno peggio perché non si ha il tempo di addestrarle adeguatamente.
<img src="/assets/img/2019/qr-saor.png" class="qr">
I nuovi arrivi sono gestiti come le sarde in *Saor*: gli si dà giusto un'infarinata e poi li si butta in padella a friggere.
Metterli in affiancamento non conviene alle aziende, che dovrebbero raddoppiare le ore/uomo su progetti che hanno margini di guadagno minimi e non conviene ai *senior*, che temono di vedersi rimpiazzati da colleghi più giovani e meno costosi.
Se si leggono i dati su un grafico come quello dell'articolo di Michel Servoz, a pagina 30, sembra tutto bello: aumenta l'occupazione, diminuiscono i costi e cresce la produttività in rapporto alle ore lavorate, ma se invece della quantità consideriamo la qualità del prodotto finale, tutto cambia.
Ve lo provo con tre esempi in cui mi sono imbattuto recentemente.  
<br />  

**Esempio 1:** un paio di mesi or sono, un mio amico, nato il 29 Febbraio del 1964, si è registrato su PayPal. La maschera di registrazione non gli ha dato problemi (il 1964 fu bisestile), ma quando ha provato ad aggiornare i dati, la maschera di modifica non glielo ha permesso perché, a suo dire, la data di nascita era sbagliata. Questo vuol dire che:

- il sistema utilizza due funzioni distinte per il controllo della data di nascita in inserimento e in modifica (errore: dovrebbero essere uguali);

- la funzione utlizzata in modifica non è una funzione standard, ma è codice scritto *ad-hoc* (errore: si dovrebbe utilizzare una funzione standard affidabile);

- non è stato fatto un test accurato della maschera di modifica (errore: la corretta gestione dei casi particolari, come gli anni bisestili, va sempre verificata).
<br />

**Esempio 2:** nel sistema di richiesta finanziamenti della mia banca, il campo per l'inserimento dell'indirizzo è limitato a un massimo di trenta caratteri. Modificando il codice HTML della pagina all'interno del browser, si può inserire un dato più lungo del previsto, ma al passo successivo della procedura il sistema va in errore e mostra dei dettagli sulla configurazione del sistema.
Gli errori, in questo caso, sono tre:

- la lunghezza del campo: in trenta caratteri non entra nemmeno: *Via Circonvallazione Gianicolense*;
- il mancato controllo del valore inserito dall'utente;
- la visualizzazione di informazioni sul sistema.

In altre parole, il sistema non è stato sottoposto né a un test funzionale, né a un test di sicurezza accurato. Che qualcuno sbagli a scrivere la dimensione massima di un campo è accettabile; che nessuno lo controlli, no.  
<br />

**Esempio 3:** se si prova a cambiare la *password* di *default* di un router ADSL di TIM, il sistema non lo permette perché, a suo dire, la *password* non è valida.
Lo stesso errore si verifica anche cercando di salvare la password di default (*admin*).  
<br />
Ho scelto questi tre esempi perché si tratta di sistemi non banali (due sistemi di pagamento *on-line* e un sistema di autenticazione), che svolgono correttamente le loro funzioni primarie, ma solo a patto che l'utente non faccia nulla di anomalo: la minima deviazione dal percorso ideale definito dall'analisi funzionale può metterli in crisi.
<img src="/assets/img/2019/qr-resilienza.png" class="qr">
Samuele Dominioni, nel suo articolo “L'Europa nell'Iperstoria” (p. 106), citando il documento *Resilienza, deterrenza e difesa: verso una cibersicurezza forte per l'UE*, dell'Alto Rappresentante dell'Unione per gli Affari Esteri e la politica di sicurezza, afferma - correttamente - che la *resilienza*, ovvero *la capacità di anticipare, resistere, recuperare e adattarsi a condizioni avverse, stress, attacchi o compromessi delle risorse informatiche*<a href="#mitre" class="nota" name="fn-8">8</a>

> è alla base “di un ecosistema cibernetico affidabile, sicuro e aperto”

La maggior parte dei sistemi che utilizziamo oggi, però, è tutt'altro che resiliente. Il nocciolo del programma, realizzato da personale esperto, svolge correttamente delle mansioni estremamente sofisticate come l'autenticazione a due fattori o la crittografia con chiavi asimmetriche, ma tutto quello che c'è intorno, realizzato da personale meno capace, è fragile.
L'Intelligenza Artificiale, per ora, è ancora un prodotto (relativamente) poco diffuso e chi se ne occupa ha le competenze necessarie per svolgere un buon lavoro, ma quando la domanda di tecnici aumenterà, sono sicuro che vedremo ripetersi ciò che è successo già tre volte negli ultimi trent'anni: un proliferare indiscriminato di *Artificial Intelligence Architect* e *Data Scientist* provenienti da settori quanto mai disparati che affogheranno degli algoritmi estremamente sofisticati e affidabili in funzioni di interfaccia maldestre e instabili, rimpinzandoli con dati inconsistenti.
E nella *smart-car*, ci saremo noi.

## Il bordo
Il termine *etica*, così come: *amore* e *arte* è una parola di cinque lettere che inizia e finisce con una vocale.
Così come *amore* e *arte*, inoltre, *etica* è un termine che si utilizza spesso senza darne una definizione rigorosa.
Il problema è che di etica, al contrario della mamma (sempre cinque lettere, ma l'iniziale è una consonante), non ce n'è una sola.
O meglio: l'ètica in quanto:

> dottrina o riflessione speculativa intorno al comportamento pratico dell’uomo, soprattutto in quanto intenda indicare quale sia il vero bene e quali i mezzi atti a conseguirlo:

è indubbiamente una; ma se invece, come in questo caso, ci riferiamo a quel:

>complesso di norme morali e di costume che identificano un preciso comportamento nella vita di relazione con riferimento a particolari situazioni storiche<a href="#etica" class="nota" name="fn-9">9</a>

<img src="/assets/img/2019/qr-capgemini.png" class="qr">dobbiamo necessariamente specificare a *quale* etica ci stiamo riferendo, perché, come illustra chiaramente l'articolo di Edoardo Campanella “Competizione hi-tech: una guerra di nervi” (p. 47) e quello, già citato di Samuele Dominioni, l'etica degli Stati Uniti, della Cina e dell'Europa si basano su valori e regole piuttosto diversi fra loro.
Nel rapporto di Capgemini *Why addressing ethical questions in AI will benefit organizations*, leggiamo che:

> AI interactions that consumers and citizens perceive<a href="#perceive" class="nota" name="fn-10">10</a> as ethical build trust and satisfaction

Bene, ma cosa si intende per: *ethical*?
Se cerchiamo la radice "ethic" all'interno del testo, scopriamo che compare 243 volte, ma per leggere una definizione non tautologica del temine, dobbiamo arrivare alla centottantesima ricorrenza, a pagina 17:

> What does it mean to implement digital and AI ethics? Beyond what is legal or not – and therefore what you must comply with – you need to determine what you stand for as an organization – what are your brand values? These values should exist whether you use AI or not. You can then define your ethical AI code on the basis of what you stand for; what that implies for how you think of the impact of your decisions on your company, employees, customers, and society at large; and, as a result, determine what kind of AI practices you can deem conformant to your ethics.

Questa cristallina definizione di Nicolas Economou, però, ci riporta al punto di partenza, perché i *brand values* variano da azienda ad azienda e da nazione a nazione, il che vuol dire che ciascuna azienda e ciascuna nazione farà una crostata con un “bordo” diverso: più alto o più basso, più morbido o più croccante a seconda dei casi.
<img src="/assets/img/2019/qr-springer.png" class="qr">
È possibile definire un insieme condiviso di valori e di regole, per far sì che ci sia un minimo di uniformità nella produzione di crostate?
Temo di no, almeno, non ora.
Qualche tempo fa, stavo cercando un saggio sul *brainjacking*<a href="# brainjacking] su *Springer*[^springer" class="nota" name="fn-11">11</a> e nell'elenco degli articoli mi sono apparsi due titoli in sequenza:

<img src="/assets/img/2019/value-sensitive-design.png" style="width:100%;padding:1rem 0">

In sostanza, è da vent'anni che si sta lavorando a del software “sensibile ai valori”, ma ancora non si è riuscito a decidere *quali* debbano essere questi valori.

## Il reticolo
La giurisprudenza ha sempre inseguito le innovazioni tecnologiche:

- la prima Legge sul *copyright*, il cosiddetto: *Statute of Anne*, risale al 1710, duecentocinquantacinque anni dopo l'invenzione del torchio da stampa;

- il provvedimento del Garante della privacy relativo agli amministratori di sistema fu pubblicato nella Gazzetta Ufficiale del 24 Dicembre 2008, quando Internet contava più di 186 milioni di siti Web e un miliardo e mezzo di utenti in tutto il Mondo;
<img src="/assets/img/2019/qr-europarl.png" class="qr">

- la *General Data Protection Regulation* (GDPR) è entrata in vigore il 24 Maggio del 2016, nove anni dopo il rilascio del primo iPhone.

La regolamentazione dell'Intelligenza Artificiale è stata più solerte:
come riporta l'articolo “La roboetica e le differenze transatlantiche” di Roberto Cingolani,
<img src="/assets/img/2019/qr-obama.png" class="qr">
la *Robohetics Road Map* di Giammarco Verrugio è stata pubblicata nel 2006; le *Norme di diritto civile sulla robotica* della Commissione Affari Legali del Parlamento europeo sono state presentate a Maggio del 2015, mentre a Ottobre del 2016, l'Office of Science and Technology Policy della Casa Bianca ha pubblicato il rapporto: *Artificial Intelligence, Automation, and the Economy*.
<img src="/assets/img/2019/qr-rules.png" class="qr">
Già nel 2010, poi, l'Engineering and Physical Sciences Research Council (EPSRC)
aveva definito cinque *Principles of robotics*, che però, nella nostra allegoria dolciaria, sono più adatti a far parte del bordo, perché, malgrado l'utilizzo del verbo *Dovere* al condizionale, cercano di delimitare i confini dell'AI:

1. *I robot sono strumenti multiuso. Non dovrebbero essere progettati esclusivamente o principalmente per uccidere o danneggiare gli esseri umani, tranne che nell'interesse della sicurezza nazionale.*

2. *Gli umani, non i robot, sono gli agenti responsabili. I robot dovrebbero essere progettati e utilizzati per quanto possibile in conformità alle leggi e ai diritti e libertà fondamentali, inclusa la privacy.*

3. *I robot sono prodotti. Dovrebbero essere progettati utilizzando processi che garantiscano la loro sicurezza e affidabilità.*

4. *I robot sono esseri artificiali. Non dovrebbero essere progettati in modo ingannevole per sfruttare gli utenti vulnerabili; invece la loro natura di macchina dovrebbe essere evidente.*

5. *La responsabilità legale per un robot dovrebbe essere attribuita a una persona.*

Mi permetto di aggiungerne una sesta, di ordine pratico:

<ol start="6">
<li style="font-style:italic">I robot dovrebbero essere costruiti con materiali il più possibile riciclabili, alrimenti, fra dieci anni avremo il problema del loro smaltimento.</li>
</ol>

Comunque, anche se le Leggi ci sono, o ci saranno, rimane il problema della loro applicazione.
Sono passati più di tre anni dall'entrata in vigore della GDPR e più di un anno dal termine del periodo di transizione concesso alle Compagnie per adeguarsi alle nuove disposizioni, ma continuiamo a ricevere telefonate di *telemarketing* e messaggi di SPAM.
Sono passati undici anni dalla pubblicazione del Provvedimento sugli amministratori di sistema, ma continuano a esserci dei server con utenze di amministrazione anonime.
Sono passati trecentonove anni dallo Statuto di Anna, ma la maggior parte delle vignette umoristiche che vediamo sui *social network* sono palesi violazioni del diritto d'autore<a href="#copertina" class="nota" name="fn-12">12</a>.
Questo non deve avvenire con l'Intelligenza Artificiale.
Il malefico Smartphone e il suo sordido complice Socialnetwork ci hanno presi alla sprovvista: con i loro *friend* e i loro *like* si sono finti nostri amici, per poi ridurci a un esercito di zombie<a href="#zombie" class="nota" name="fn-13">13</a>; la triade *AI*, *IoT* e *BigData* può fare molto di peggio, se non glielo impediamo, perché quei *big-data* altro non sono che le nostre vite: ciò che facciamo, ciò che diciamo, le persone che amiamo.
Il reticolo di regole dell'AI non deve solo mettere in pratica i princìpi etici del bordo, deve anche tutelarli.
Una crostata priva di bordo e di reticolo posso accettarla da Gianluca Fusto<a href="#fusto" class="nota" name="fn-14">14</a>; se me la offre Jack Ma, non posso fare a meno di chiedermi dove sia la fregatura<a href="#ma" class="nota" name="fn-15">15</a>.
Il problema &mdash; e, in ultima analisi, la causa scatenante di questo documento &mdash; è che noi abbiamo i princìpi, ma Jack ha i soldi. Tanti.
Riusciranno le idee del nostro vecchio Continente, non dico a imporsi (ché sarebbe sbagliato), ma almeno a sopravvivere allo scontro con le *mere stackeholders preferences* degli altri attori di questo poema epico?

## Conclusioni
Ho <img src="/assets/img/2019/qr-dolls.png" class="qr"> letto diverse ipotesi fanta-paranoidi sui possibili lati negativi dell'Intelligenza Artificiale, ma tutte presupponevano un errore o un dolo, mentre ciò che oggi mi fa più paura non è l'ipotesi di essere strangolato da un robot sessuale, ma la pubblicità di un sistema di riconoscimento delle emozioni di una platea. La possibilità, per un relatore, di conoscere *in tempo reale* le reazioni dell'uditorio alle sue affermazioni, inverte lo scopo della comunicazione: non è più il pubblico che acquisisce informazioni, le introietta ed eventualmente adatta il suo comportamento al nuovo paradigma, ma è chi parla che impara i gusti del suo pubblico e adatta le sue affermazioni in modo da incontrare il massimo di gradimento possibile.
Questo è un comportamento accettabile per un *social-media-manager* o un *instagrammer*, che altrimenti sarebbero costretti ad andare a lavorare per guadagnarsi il pane, ma è inammissibile per un conferenziere o un politico, che dovrebbero guidare, non essere guidati.
So perfettamente che tutto questo avviene già, in altre forme, ma, per quanto siano entrambe azioni esecrabili, c'è una bella differenza fra una valutazione fatta a posteriori, basandosi sull'esito dei sondaggi e una valutazione fatta sul momento, da un sistema automatizzato.
Tutto ciò che ci disabitua a pensare è sbagliato, perché ci rende stupidi.
Il progresso tecnologico ci ha dato tanto, ma tanto anche ci ha preso: non sappiamo più descrivere perché facciamo foto; non *con*versiamo più, perché comunichiamo a messaggi; non sappiamo più dare indicazioni stradali perché usiamo i GPS<a href="#gps" class="nota" name="fn-15">15</a>.
Quante cose disimpareremo, a causa dell'AI?
Riusciremo a far sì che l'intelligenza artificiale non favorisca la stupidità naturale e che l'*iper-storia* non produca un'*ipo-umanità*?  
Dipende da noi.  
Buon lavoro.  

<section class="note">
    <h2>Note</h2>
    <ol>
        <li>
            <a target="note"
             href="http://www.aspeninstitute.it/aspenia/numero/la-politica-dellalgoritmo">
                 www.aspeninstitute.it/aspenia/numero/la-politica-dellalgoritmo.</a>
            <a href="#fn-1" name="aspenia">&#8617;</a>
        </li>
        <li>
            <a target="note"
             href="http://www.amazon.com/dp/B07WQGSY6N">
                www.amazon.com/dp/B07WQGSY6N</a>.
            <a href="#fn-2" name="floridi-f">&#8617;</a>
        </li>
        <li>
            <a target="note"
             href="http://www.propublica.org/article/how-we-analyzed-the-compas-recidivism-algorithm">
                www.propublica.org/article/how-we-analyzed-the-compas-recidivism-algorithm</a>.
            <a href="#fn-3" name="ProPublica">&#8617;</a>
        </li>
        <li>
            <a target="note"
             href="http://gpt2.apps.allenai.org">
                gpt2.apps.allenai.org</a>.
            <a href="#fn-4" name="allenai">&#8617;</a>
        </li>
        <li>
            <a target="note"
             href="http://www.pewinternet.org/2018/12/10/improvements-ahead-how-humans-and-ai-might-evolve-together-in-the-next-decade">
                www.pewinternet.org/2018/12/10/improvements-ahead-how-humans-and-ai-might-evolve-together-in-the-next-decade</a>
            <a href="#fn-5" name="pew">&#8617;</a>
        </li>
        <li>
             <i>Moana</i>, di Marco Giusti - Mondadori, 2004.
            <a href="#fn-6" name="moana">&#8617;</a>
        </li>
        <li>
            MITRE, <i>Cyber Resiliency Metrics Catalog</i>, Settembre 2018.
            <a target="note"
             href="http://www.mitre.org/publications/technical-papers/cyber-resiliency-metrics-catalog">
                www.mitre.org/publications/technical-papers/cyber-resiliency-metrics-catalog</a>
            <a href="#fn-7" name="mitre">&#8617;</a>
        </li>
        <li>
            <a target="note"
             href="http://www.treccani.it/vocabolario/etica">
                http://www.treccani.it/vocabolario/etica</a>.
            <a href="#fn-8" name="etica">&#8617;</a>
        </li>
        <li>
            Trovo interessante l'utilizzo di questo verbo, come a suggerire che
            l'eticità di un sistema non debba necessariamente essere caratteristica
            reale, ma basta che sia percepita come tale dall'utente.
            <a href="#fn-9" name="perceive">&#8617;</a>
        </li>

        <li>
            L'intromissione malevola nel funzionamento di una protesi cerebrale,
            per alterare il comportamento del paziente.
            <a href="#fn-10" name="brainjacking">&#8617;</a>
        </li>
        <li>
            <a target="note"
             href="http://rd.springer.com">
                rd.springer.com</a>.
            <a href="#fn-11" name="springer">&#8617;</a>
        </li>
        <li>
            Così come l'immagine che ho pubblicato in copertina, se non mi ricordo
            di scrivere al National Post per chiedere il permesso di utilizzarla.
            <a href="#fn-12" name="copertina">&#8617;</a>
        </li>
        <li>
            Padre, madre, figlio e figlia che sono a tavola insieme, ma guardano
            ciascuno il loro <i>smart-phone</i> non sono una famiglia, sono una <i>botnet</i> .            
            <a href="#fn-13" name="zombie">&#8617;</a>
        </li>
        <li>
            Gianluca Fusto è stato per le crostate quello che i Beatles
            sono stati per la musica Pop.
            <a target="note"
             href="https://gianlucafusto.com">
                gianlucafusto.com</a>
            <a href="#fn-14" name="">&#8617;</a>
        </li>
        <li>
            L'immagine in copertina è una reazione alle recenti affermazioni di Ma
            alla Conferenza mondiale sull’intelligenza artificiale di Shanghai.
            <a href="#fn-15" name="ma">&#8617;</a>
        </li>
        <li>
            Vero è che, quando le davamo, non le stavamo a sentire..
            <a href="#fn-16" name="gps">&#8617;</a>
        </li>
    </ol>
</section>
