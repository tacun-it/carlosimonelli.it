---
category:   "sicurezza"
excerpt:    "Conseguenze di una risposta armata a un attacco informatico"
h1:         "Hacker is the new ISIS"
h2:         "Conseguenze di una risposta armata a un attacco informatico"
layout:     post
image:      "/assets/img/2019/hacker-is-the-new-isis.jpg"
thumbnail:  "/assets/img/2019/hacker-is-the-new-isis-m.jpg"
img-alt:    "Manifesto della propaganda inglese durante la II Guerra Mondiale"
permalink:  "/sicurezza/hacker-is-the-new-isis.html" 
tags:       [hacker, ISIS, Israele, Hamas]
title:      "Hacker is the new ISIS"
---

La guerra è cambiata.
Come nel 1379, quando l'esercito veneziano riconquistò la torre delle Bebbe, in mano ai Genovesi, utilizzando per la prima volta dei razzi a polvere nera<a class="nota" href="#rocchetta" name="fn-1">1</a>.
O nel 1915, quando due dirigibili Zeppelin tedeschi portarono a termine il primo bombardamento aereo sulle città inglesi di King’s Lynn e Great Yarmouth. O nel 1945, quando fu sganciata la prima bomba atomica su Hiroshima.
Come forse avrete intuito, non è cambiata in meglio.  

<img class="illustrazione cornice" src="{{ page.image }}" alt="{{ page.img-alt }}" >

Il 5 Maggio scorso, Israele ha risposto con un lancio di missili a una: "<a href="https://twitter.com/IDF/status/1125066395010699264" rel="noopener noreferrer" target="_blank">Hamas cyber offensive against Israeli targets</a>", diventando così la prima nazione ad aver utilizzato delle armi convenzionali in risposta a un attacco digitale. La NATO aveva anticipato questa possibilità nel 2015, <a href="https://www.nato.int/cps/en/natohq/topics_78170.htm" rel="noopener noreferrer" target="_blank">dichiarando il cyberspazio un territorio di guerra a tutti gli effetti</a> e, come è stato fatto notare da alcuni commentatori, c'è stato un possibile precedente con l'uccisione dell'hacker <a href="https://www.nytimes.com/2016/11/24/world/middleeast/isis-recruiters-social-media.html" rel="noopener noreferrer" target="_blank">Junaid Hussain</a> da parte di un drone, che però è stata un'operazione di *intelligence*, non di guerra. Questa volta, al contrario, la reazione è stata immediata: gli hacker di Hamas hanno attaccato Israele e Israele, in risposta, ha lanciato un missile contro la casa da cui ritenevano che partisse l'attacco.  
  
Si era parlato della legittimità di questa possibilità a un Security Summit di qualche anno fa.
L'ipotesi che aveva fatto il professor Ziccardi era la seguente: se la nazione A colpisce con un razzo una centrale elettrica della nazione B, oltre al danno diretto dovuto alla distruzione dell'edificio, può causare una serie di danni collaterali altrettanto gravi, come il blocco delle attività produttive o degli ospedali.
Se, invece, la nazione A causa il blocco della stessa centrale elettrica con un attacco informatico, non causa alcun danno strutturale, ma può causare gli stessi effetti collaterali di un attacco missilistico.
In questo caso, la nazione B ha il diritto di rispondere all'attacco con il lancio di un missile o sarebbe una violazione del principio di proporzionalità dell'<a href="https://casebook.icrc.org/glossary/proportionality" rel="noopener noreferrer" target="_blank">International Humanitarian Law</a>?  
In teoria, tutto dipende dal danno subìto: se è equivalente a quello che sarebbe stato causato da un attacco convenzionale, la nazione B avrebbe il diritto di rispondere militarmente.
In pratica, il problema è che il missile lo sai da dove parte, chi lo lancia e che cosa colpisce; l'attacco informatico, no.
Quanto meno, non sempre.  
  
Paradossalmente, più l'attacco informatico è grave, e quindi "meritevole" di risposta armata, meno è comprovabile, perché per farlo si dovrebbero rendere pubbliche delle informazioni che non è possibile rivelare.
Se Hamas avesse attaccato il sito di un fan club di Barbra Streisand, Israele avrebbe potuto tranquillamente pubblicare i file di <em>log</em> del sistema attaccato, dimostrando sia la provenienza dell'attacco che l'entità dei danni subiti.
Il tweet dell'<em>Israel Defense Forces</em>, però, non parla di Barbra, anzi: non specifica né quali fossero gli <em>Israeli targets</em> dell'attacco né quali siano stati i danni subiti, prima della: *successful cyber defensive operation*.
In altre parole, la legittimità dell'attacco convenzionale è basata su un'auto-certificazione: l'IDF dice che c'è stato un attacco, che proveniva da un determinato edificio e che i danni che ne sono derivati, o che ne <em>potevano</em> derivare, giustificavano la distruzione dell'edificio e dei suoi occupanti con un missile.
Comodo.  
  
Non intendo mettere in discussione le ragioni di Israele: solo le persone coinvolte direttamente nell'evento (alcune delle quali, defunte) possono sapere se è stata una reazione adeguata o no; quello che mi preoccupa sono le possibili ricadute civili di questo modo di pensare; in particolare, l'applicazione alla sicurezza interna del principio:

> È un hacker, non c'è bisogno di prove

Come tutte le invenzioni umane - dalla dinamite alla morfina, dalle automobili ai farinacei -, la sicurezza può essere o buona o cattiva a seconda dell'uso che se ne fa: può proteggere la vita dei cittadini, facendo sì che il loro volo finisca sulla pista di un aeroporto e non sulla facciata di un grattacielo, o può essere presa come pretesto per delle violazioni più o meno gravi delle loro libertà personali.
Per essere giustificata, comunque, la sicurezza ha bisogno di un pericolo.
Indossare il casco, allacciare la cintura di sicurezza, scegliere delle password complicate sono tutte seccature, ma le accettiamo perché sappiamo che ci proteggono da rischi potenziali sicuramente più gravi e fastidiosi.
Quindi, per giustificare norme di sicurezza eccezionali, abbiamo bisogno di pericoli eccezionali.  
  
In quest'ottica, gli estremisti islamici non sono malaccio come Grande Satana, ma hanno dei difetti innegabili: il primo è che, lasciati a loro stessi, tendono a uccidere le persone, cosa che alla lunga può diventare imbarazzante; in secondo luogo, sono un gruppo di persone con caratteristiche comuni, che li rendono facilmente identificabili.
Accusare qualcuno di essere un estremista islamico senza prove concrete non è così facile; al contrario, chiunque sappia cosa fa l'istruzione:

<pre>curl https://www.humanrights.com &gt; /dev/null 2&gt;&amp;1 &amp;</pre>

o abbia una felpa col cappuccio nel suo guardaroba potrebbe a buon diritto essere accusato di essere un hacker.
Per esempio, se qualcuno mi accusasse di essere un estremista islamico a causa delle citazioni dal *Mantiq al-tair*<a class="nota" href="#mantiq" name="fn-2">2</a>. che ho pubblicato sulla mia pagina Facebook, potrei scagionarmi grazie alla mia militanza più che decennale in una setta di motociclisti adoratori del dio maiale (arrosto) e delle libagioni alcoliche.
Al contrario, se fossi accusato di essere un membro di Anonymous che recluta e addestra giovani hacker da utilizzare poi per attacchi a obiettivi istituzionali, l'unica cosa che potrei fare sarebbe di aggiungere la voce:

> 2013-corrente: Anonymous - Selezione del personale; formazione; attacchi a obiettivi istituzionali.

al mio profilo su Linkedin, perché basterebbe la testimonianza di un agente infiltrato, vero o presunto tale, per farmi finire in gattabuia.  
  
È questo, che mi spaventa della minaccia hacker: è la scusa perfetta.
Non ha bisogno di nulla, né di un attacco, né di un colpevole.
Può riguardare ciascuno di noi e, al di là dei missili, può giustificare una gamma di deterrenti o punizioni che vanno dalla sorveglianza delle comunicazioni alla prigionia.
Certo: in questo momento, gli attacchi informatici non hanno l'<em>appeal</em> mediatico delle famose armi di distruzione di massa di Saddam, ma appena la <em>Internet Of Things</em> si sarà diffusa adeguatamente e ciascuno di noi avrà tutti i suoi elettrodomestici collegati in Rete, basterà spegnere qualche frigorifero o qualche scaldabagno qua e là per ottenere un immediato e incondizionato appoggio dall'opinione pubblica all'<em>hackeromachia</em>.  
Scommettiamo?

<section class="note">
    <h2>Note</h2>
    <ol>
        <li>
            Che, a causa della loro somiglianza con le rocchette per filare,
            furono chiamati, appunto: "rocchette", da cui derivarono poi i
            termini: <em>rocket</em> e <em>Rakete</em>.
            <a href="#fn-1" name="rocchetta">&#8617;</a>
        </li>
        <li>
            <i>Il verbo degli Uccelli</i>, poema Persiano del XIII Secolo.
            <a href="#fn-2" name="mantiq">&#8617;</a>
        </li>
    </ol>
</section>
