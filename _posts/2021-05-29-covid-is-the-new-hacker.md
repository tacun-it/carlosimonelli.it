---
category:   "sicurezza"
excerpt:    "Si può combattere un virus biologico come se fosse un virus informatico?"
h1:         "Covid is the new hacker"
h2:         "Si può combattere un virus come se fosse un virus?"
layout:     post
image:      "/assets/img/2021/gaeta-pianta.jpg"
thumbnail:  "/assets/img/2021/gaeta-pianta.jpg"
img-alt:    "Pianta della roccaforte di Gaeta"
permalink:  "/sicurezza/covid-is-the-new-hacker.html" 
tags:       [cybersecurity, Gaeta, COVID]
title:      "Covid is the new hacker"
---

<section class="citazione">
E non verranno i Piemontesi, ad assalire Gaeta<br />
Con le loro Land Rover, con le loro Toyota.<br />
<span>R. Vecchioni - </span>Dentro gli occhi
</section>

Essere un ossessivo-compulsivo con una leggera tendenza alla paranoia, se ti guadagni da vivere facendo l'esperto di sicurezza, è un bene; le medesime peculiarità caratteriali, al contrario, sono decisamente un male quando alle 21:55 la tua donna di servizio ti scrive:

> Ho fatto un molecolare e sono risultata positiva

Non potrai farti un tampone prima delle 8:00 dell'indomani quindi sai che ti aspettano almeno dieci ore di panico controllato.
Qualcuna di meno, se riesci ad addormentarti.
Cerchi di distrarti guardando la televisione, ma l'ennesimo thriller con Jason Statham, intervallato da pubblicità di ansiolitici (un conflitto di interessi che ti riprometti di studiare con più attenzione, se sopravvivi), non fa che aumentare la tua agitazione; così, spegni il televisore, ti prepari una *tisana relax*, leggi un po' e poi cerchi di dormire.
Appena chiudi gli occhi, però, il tuo cervello comincia a fare ciò che sa fare meglio, ovvero analizzare ciclicamente tutte le possibili conseguenze di un determinato evento; solo che stavolta, quello che potrebbe essere infetto non è un computer, sei tu.  
Notizia buona: non hai sintomi.
Notizia cattiva: hai visto la tua donna di servizio solo quattro giorni prima, quindi i giochi sono ancora aperti.
Ripensi a tutto quello che è successo l'ultima volta che è venuta a casa tua.
Ovviamente, appena lei è arrivata hai indossato la mascherina, ma non l'hai tenuta tutto il tempo; quando lei è venuta a pulire il pavimento nella stanza in cui lavoravi, per esempio, non ce l'avevi, te la sei messa quando ti sei accorto che lei si stava avvicinando. 
Sarà stato troppo tardi?
Anche non lo fosse stato, lei ha pulito sia la cucina che il bagno: le posate e i piatti in cui mangi così come il tuo spazzolino da denti.
Sì, d'accordo: le probabilità di un contagio da contatto sono piuttosto basse, ma non nulle.  
Ti chiedi, rabbioso, come sia possibile che un professionista della sicurezza come te si sia fatto fregare così facilmente.
Dov'era, la tua paranoia, quando avevi bisogno di lei?
In ossequio alle cinque fasi del lutto della dottoressa Kübler-Ross, al rifiuto e alla rabbia segue la contrattazione: prometti a te stesso e al Cielo che, se sopravvivi, farai molta più attenzione a come ti comporti e applicherai alla tua vita le stesse buone prassi che applichi quotidianamente nel tuo lavoro.
Da qui a chiedersi se sia possibile sfruttare i princìpi della sicurezza informatica per proteggersi dal COVID, il passo è breve e siccome questo nuovo problema generico ti distrae da quello che ti riguarda personalmente, ti concentri su di lui.
<br />    
Con il termine: **superficie di attacco** si intende l'insieme dei punti di un sistema che sono direttamente accessibili a un eventuale attaccante.
Una casa che abbia una sola porta d'ingresso ha una superficie di attacco minore di una casa che abbia anche una porta di servizio &mdash;  a parità di finestre, ovviamente.  
La **riduzione della superficie di attacco** è uno dei princìpi fondamentali della sicurezza informatica ed è anche il motivo per cui un bunker è a forma di bunker e non di Colosseo: una piccola cupola di cemento con un sola porta blindata e strette feritoie al posto delle finestre è molto difficile da attaccare, perché i possibili punti di ingresso sono pochi e ben difesi.  
Nel caso dei server Web, la superficie di attacco si contiene limitando il numero dei canali di accesso al sistema, lasciando aperti, di solito, solo quello per l'accesso degli amministratori del sistema e quello per la connessione al sito.
Se invece stiamo parlando di un telefono o di un computer, la superficie di attacco si riduce installando nel sistema solo il software strettamente necessario al suo utilizzo e disinstallando o quando meno disabilitando i programmi che non sono necessari &mdash; il contrario di quello che fa l'utente medio, per capirsi.  
In ottica pandemica, la nostra superficie di attacco varia in funzione del numero di  persone che frequentiamo in maniera conviviale (leggi: senza indossare la mascherina) e del numero di persone che i nostri amici frequentano a loro volta nel lasso di tempo massimo di incubazione del virus.
La riduzione della superficie di attacco si ottiene, quindi, riducendo i rapporti sociali al minimo indispensabile e utilizzando la mascherina quanto più possibile.
Nel mio caso, dalla fine della quarantena, mi capita di mangiare al chiuso solo con un numero ristretto di persone: due sono vaccinate; altre due hanno già avuto il COVID; alte due non si sono ammalate pur avendo avuto contatti con persone infette, quindi mi illudo che abbiano delle buone difese immunitarie. 
Il problema è la settima persona: un mio amico che fa il fotografo sportivo e che quindi comporta una considerevole estensione della superficie di attacco totale, non tanto per le sue frequentazioni professionali quanto per le femmine che lo insidiano, roba che nemmeno Carl Perkins ai bei tempi.
Frequentarlo, è un'abiura del principio della *Zero Trust*, che andremo ora a illustrare.
<br />    
Il principio della **Zero Trust** è un'altra regola della sicurezza informatica che può proteggerci dal COVID.
Il suo significato è chiaro: non ti devi fidare di nessuno, nemmeno delle persone fidate.
In tempi andati si riteneva che il traffico proveniente dalla rete interna della propria società fosse sempre affidabile e i computer all'interno del perimetro aziendale avevano molto spesso libero accesso ai server.
Non c'è niente di più sbagliato, perché quello che è sicuro oggi non è detto che lo sarà anche domani.  
La roccaforte di Gaeta, per lungo tempo, fu considerata estremamente sicura perché la sua superficie di attacco, costituita dall'istmo della spiaggia di Serapo, era più stretta della linea di difesa delle fortificazioni.
L'adozione dei cannoni a canna rigata, da parte dell'Esercito piemontese, nel 1860, annullò questo vantaggio perché permise ai sabaudi di cannoneggiare la rocca da lontano, senza il bisogno di attaccarla con la fanteria.  

<img class="illustrazione cornice" src="/assets/img/2021/gaeta-assedio.jpg" alt="Gaeta durante l'assedio" title="Gaeta durante l'assedio" style="height:360px">

I dipendenti di una società potrebbero non essere soddisfatti del loro lavoro (magari si sono visti negare un aumento o una promozione o, peggio, li hanno licenziati); o, pure, potrebbero essere stati ingannati da qualcuno che ha rubato le loro credenziali di accesso ai sistemi; o, ancora più semplicemente, potrebbero essere stupidi.
Lasciar loro libero accesso alle risorse critiche dell'azienda è un errore da non fare mai.  
Alcuni modi per applicare la *Zero Trust* nella vita di tutti i giorni sono:

- non lasciare la propria carta di credito al cameriere, ma andare personalmente a pagare;
- non lasciare il proprio cellulare in riparazione senza averlo prima resettato cancellando tutti i dati;
- non condividere le proprie credenziali di accesso a servizi di posta o ai social-network con altre persone, meno che mai se ci contattano tramite messaggi o telefonicamente;
- non digitare il PIN del proprio Bancomat se qualcuno può vederlo.

Un'applicazione più specialistica della *Zero Trust* è il modo in cui gestisco gli indirizzi di posta che utilizzo come *username* per la connessione ai siti Web.
Invece di utilizzare un unico indirizzo per tutti i server, creo ogni volta un *alias* di posta (ovvero un indirizzo che inoltra tutti i messaggi in arrivo a un'altra casella di posta) con il nome del server presso cui mi sto registrando:

> facebook@..  
twitter@..  
youporn@..  

Questo approccio, anche se macchinoso, ha due lati positivi (tre, se ci aggiungiamo il fatto che è macchinoso): il primo è che se mi dovesse arrivare SPAM da uno di questi indirizzi saprei quale acconto è stato violato; il secondo lato positivo è che, in quel caso, potrei eliminare l'alias compromesso e crearne uno nuovo senza che questo mi causi alcun problema con la posta quotidiana.  
Applicare il principio della *Zero Trust* alla lotta contro il COVID è molto più semplice: basta non fidarsi di nessuno.
Se non mi fossi fidato della mia donna di servizio, se fossi uscito mentre lei lavorava in casa e avessi lasciato le finestre aperte dopo che era andata via, mi sarei risparmiato una settimana di ansia.
Non vi fidate nemmeno, anzi: soprattutto, della la vostra “rete interna” di amicizie, perché, come abbiamo visto, sono loro il vostro principale punto debole.
Ieri, per esempio, ho visto il mio amico fotografo per un aperitivo, ma eravamo in un bar sul mare e quando lui ha detto: «Mi siedo qui, ché non ho il sole in faccia», ho risposto: «Basta che stai sottovento..»  
L'aspetto negativo della *Zero Trust* è che può rendervi poco simpatici; il lato negativo del COVID è che può rendervi molto morti. Decidete voi.
<br />  
La **bonifica dei parametri in ingresso** è un altro principio fondamentale della sicurezza, ma è meno facile da spiegare.
In soldoni, consiste nel “ripulire” i dati che un server Web riceve dall'esterno per prevenire il rischio che siano stati alterati in modo da modificare il comportamento del sistema.  
Quando inserite il vostro nome utente (per esempio: “pippo”) e la vostra password (per esempio: “12345”) in una maschera di *login*, il server Web riceve i valori che avete scritto e li inserisce all'interno di un'istruzione che cerca tutti i record della tabella **utenti** che abbiano uno *username* e una password uguali a quelli che avete digitato:

<blockquote class="sql">
SELECT * FROM utenti <br/>
WHERE username = '<b>pippo</b>' <br/>
AND password = '<b>12345</b>';
</blockquote>

Se tutto va bene, il sistema troverà i vostri dati e li visualizzerà nella pagina.
Se però un utente malintenzionato inserisce come username la stringa: “``”pluto' OR 1=1 --' "``" e il server copia questo valore nell'istruzione così com'è, il comando diventerà:

<blockquote class="sql">
SELECT * FROM utenti<br/> 
WHERE username = '<b>pluto' OR 1=1 --'</b> '<br/> 
AND password = '<b>12345</b>';
</blockquote>

che equivale a chiedere al sistema tutti i record della tabella **utenti**.  
Se provate a fare questo giochetto con uno dei vostri account, è molto probabile che l'istruzione fallirà, perché ormai quasi tutti i server Web ripuliscono i valori che ricevono dall'esterno, sostituendo i caratteri “pericolosi” come gli apici singoli, il carattere *ampersand* o i segni di minore e maggiore con dei codici HTML che ne permetteranno la visualizzazione nelle pagine Web, ma che non saranno gestiti come istruzioni dalla base-dati.  
Allo stesso modo, per ridurre i rischi di contagio, noi dobbiamo ripulire, ma non in senso lato, tutto ciò che può essere infetto.
Le mani, per prima cosa: tenete delle boccette di gel disinfettante all'ingresso di casa, nella tasca laterale dell'auto e nelle vostre borse e pulitevi le mani ogni volta che toccate qualcosa che hanno toccato anche altre persone: un bancomat, la pompa di benzina, il resto al bar.  
È una buona idea anche pulire tutto quello che facciamo entrare in casa.
Quando eravamo ancora in quarantena, io toglievo i cartoni di imballaggio alle bottiglie di birra o ai barattoli di sugo e li buttavo nel cassonetto prima di mettere la spesa in auto.
Ora lo faccio quando arrivo a casa, ma poi pulisco tutto &mdash; bottiglie, barattoli, yogurt, frutta e verdura &mdash; con acqua e aceto o con un detergente alla candeggina.  
Male non fa, visto che frutta e verdura andrebbero lavate comunque, dà un senso di sicurezza ed è un'applicazione del prossimo precetto, ovvero le **linee di difesa multiple**.
<br />  
La sicurezza è uno stato transitorio.
Così come la roccaforte di Gaeta nel 1860, ciò che oggi è sicuro potrebbe non esserlo domani o perché qualcuno ha inventato un nuovo modo per abbattere le nostre difese o perché ha trovato un modo per aggirarle.  
È necessario per ciò creare più linee difensive in modo che, se anche una di esse dovesse cadere, ci sarebbero ancora le altre a proteggere ciò che per noi è importante, siano essi dei dati o la nostra salute.  
Un errore che commettono alcuni gestori di siti Web, è di affidare tutta la sicurezza del sistema a un *firewall*, trascurando la sicurezza intrinseca dei server e del software.
In questo modo, se un attaccante riesce a superare il *firewall* (cosa tutt'altro che impossibile, checché ne dicano i venditori di *firewall*), si troverà la strada spianata verso i server.  
Un altro errore frequente (indotto da una propaganda menzognera) è quello di credere che l'anti-virus del proprio PC vi salverà da qualunque minaccia.
Pillola rossa: non è vero.
L'anti-virus riconosce **alcune** minacce e, per farlo, deve essere aggiornato praticamente ogni giorno. 
L'aggiornamento richiede una licenza valida, che ha un suo prezzo; se non paghi, l'anti-virus non si aggiorna e, da quel momento in poi, serve solo a rallentare il computer.  
Senza contare il fatto che l'anti-virus non sempre è capace di bloccare il pericolo più grande per ogni PC, ovvero l'utente stesso.
Quello che segue, è l'elenco delle chiamate fatte da un computer che ha appena visitato un sito per il download illegale di film:

<img class="illustrazione cornice" src="/assets/img/2021/altadefinizione.png" alt="Log accessi Web" title="Log accessi Web">
  
Dopo la chiamata al sito principale, il computer, **all'insaputa dell'utente** fa tutta una serie di chiamate a siti loschi o malandrini, dimostrando, una volta di più, la veridicità della frase:

> Se non paghi il servizio, il prodotto sei tu.

I siti di streaming illegale o quelli da cui è possibile scaricare le versioni gratuite dei programmi a pagamento sono tutti veicoli di infezione, né più né meno delle meretrici da strada. 
Visitarli o, peggio, fruire dei loro “servizi” è un rischio insensato, soprattutto ora che abbonarsi a un sito di streaming legale o al *Creative Cloud* di Adobe costa quanto un Mojito in un bar del Centro.  
Lo stesso atteggiamento responsabile va adottato nei confronti del COVID.
Non possiamo avere solo *una* linea difensiva, sia essa evitare gli assembramenti o indossare la mascherina o lavarsi le mani con il gel o il vaccino: dobbiamo fare **tutto** quello che è in nostro potere per limitare la diffusione del virus.  
All'inizio del 2020 era ammissibile che dovesse essere il Governo a imporci delle regole di comportamento, perché nessuno di noi, per fortuna, si era mai trovato in una situazione simile; oggi, però, dopo più di quattrocento giorni di pandemia, abbiamo imparato cosa sia corretto fare e cosa invece sia sbagliato.
Sui DPCM potrebbero scrivere solo:
<p style="text-align:center;margin:2rem;font-size:1.2rem;font-family:zapfino">Non fate cazzate.</p>
Sapremmo tutti, a cosa si riferiscono.
