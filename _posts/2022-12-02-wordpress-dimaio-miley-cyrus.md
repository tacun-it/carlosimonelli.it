---
category:   "sicurezza"
excerpt:    "Il \"defacement\" del sito di Di Maio e come i CMS hanno trasformato Internet in un campo minato"
h1:         "Wordpress, Di Maio e Miley Cyrus"
h2:         "Pregio e difetti dei Content Management System"
layout:     post
image:      "/assets/img/2022/di-maio-hack.jpg"
thumbnail:  "/assets/img/2022/di-maio-hack-m.jpg"
img-alt:    "La pagina del sito di Luigi di Maio"
permalink:  "/sicurezza/wordpress-dimaio-miley-cyrus.html" 
tags:       [cybersecurity, Luigi di Maio, Wordpress, CMS, Sucuri, Miley Cyrus]
title:      "Wordpress, Di Maio e Miley Cyrus"
---

**[Questo articolo è stato pubblicato su <a href="https://bit.ly/3Ge8m2e" target="des">Difesa & Sicurezza</a> il 2 Dicembre 2022.]**

<section class="citazione">
Fa' un programma che anche un idiota può usare<br/>e soltanto un idiota vorrà usarlo.<br /> 
<span>A. Bloch - </span> Principio di Shaw
</section>

## Introduzione
Quando ho detto a mio fratello che avevo scoperto un <a href="https://www.treccani.it/enciclopedia/defacement_%28altro%29/" target="treccani"> defacement</a> del sito Web di Luigi Di Maio, la prima reazione di Paolo è stata di chiedermi che cosa ci facessi, io, sui sito Web di Di Maio.   
È una domanda lecita e la risposta coinvolge Miley Cyrus; la scriverò alla fine, se non vado lungo con il testo.  

<a href="/assets/img/2022/di-maio-hack.jpg" name="hack"><img class="illustrazione cornice" src="{{ page.image }}" ></a>
<p class="didascalia">La home-page del sito di Di Maio. La freccia sotto la lettera "O" porta alla URL evidenziata nell'angolo in basso a sinistra</p>

Il tema di questo articolo saranno i *Content Management System* o, più brevemente, *CMS*. 
Ciò che vorrei dimostrare, laddove ce ne fosse bisogno, è che la semplificazione dei processi produttivi del software, a fronte di alcuni innegabili benefici, tende a ridurre la qualità media del software stesso; un concetto brillantemente riassunto da Arthur Bloch nel suo *Principio di Shaw*, riportato qui sopra.  

## Come è fatta una pagina Web
Se lo sapete, potete passare al paragrafo successivo, perché qui non dirò nulla che già non sappiate.  
Se, al contrario, non lo sapete, sappiate che una pagina Web è composta di due parti: una `HEAD` e un `BODY`, che nel codice sorgente della pagina appaiono così:  

```
<html>
    <head>
        …
    </head>
    <body>
        …
    </body>
</html>
```

Nel `BODY` ci sono tutti gli elementi che compongono la parte visibile della pagina: il testo, le immagini e, talvolta, il codice Javascript per le automazioni dell'interfaccia.  
L'`HEAD`, invece, contiene le informazioni che servono al vostro browser o ai motori di ricerca per gestire correttamente la pagina. 
Per esempio, l'HEAD dell'home-page di questo sito contiene, fra gli altri, i seguenti valori:

```
<head>
    <title>Home | Carlo Simonelli IT</title>
    <meta     name="thumbnail"       content="/img/thumbnail.png" />
    <meta     name="keyword"         content="informatica, cybersecurity, sicurezza" />
    <meta     name="description"     content="Opinioni di Carlo Simonelli, informatico" />
    <meta property="og:description"  content="Opinioni di Carlo Simonelli, informatico" />
    <meta property="og:title"        content="Carlo Simonelli IT - Home" />
    <meta property="og:type"         content="website" />
    <meta property="og:image"        content="/img/home.jpg" />
    <meta property="og:image:height" content="600" />
    ...
</head>
```

Ciascuno di questi elementi, chiamati “tag HTML”, fornisce un'informazione sulle caratteristiche pagina.  
Il tag `TITLE` indica il titolo che deve comparire nell'intestazione del browser e negli elenchi di Google.  
Il META tag `keyword`  contiene delle parole chiave che servono ad associare questa pagina a uno o più argomenti, per facilitare le ricerche.  
Il META tag `description` contiene una descrizione della pagina, per la visualizzazione negli elenchi dei motori di ricerca.  
I META tag con il nome che comincia per `og:` fanno parte del protocollo Open Graph (https://ogp.me) e servono per indicare a Facebook cosa debba apparire nei post che contengono la URL di una pagina Web.  
La cosa che dovete ricordare, almeno fino alla fine di questo articolo, è che **qualsiasi cosa venga scritta nei campi dell'`HEAD` non sarà mai visibile nella pagina**, ma comparirà solo nei risultati di una ricerca su Google o in un post su un *social-network*.  
Le pagine HTML sono semplici file di testo; le informazioni, tanto nell'`HEAD` che nel `BODY`, si possono inserire o a mano, se si conosce il linguaggio HTML, oppure possono essere generate da un *CMS*.  

## Cosa è un CMS

Se sapete cos'è un *Content Management System*, o, più brevemente: *CMS*, non saltate al prossimo paragrafo perché qui dirò delle cose che forse non sapete.
Ne parlerò male, ve lo dico subito, ma solo perché è oggettivamente impossibile parlarne bene.  
L'altro giorno, mentre lavoravo col mio collega Fabrizio S. a un sito realizzato con il CMS *Drupal*, ho coniato una possibile definizione di questi strumenti:

>I CMS permettono agli inetti di fare cose complesse in maniera facile e costringono le persone competenti a fare in maniera complessa delle cose facili.
 
Sembra un gioco di parole, ma è la verità.  
La realizzazione di un sito Web, attualmente, richiede *almeno* queste competenze tecniche:

- scrittura del codice HTML;
- utilizzo dei file CSS;
- gestione della SEO;
- nozioni basilari di sicurezza informatica.

Se poi il sito prevede anche contenuto dinamico, per esempio articoli da inserire periodicamente, è necessaria anche una certa competenza nella:

- gestione delle basi di dati.  

Spesso, però, le competenze di chi realizza un sito Web sono:

- so usare Photoshop (non benissimo)

I CMS suppliscono a queste carenze di base, automatizzando le attività tipiche della creazione di un sito Web:

- generazione della base-dati;
- definizione dell'interfaccia utente;
- generazione delle pagine statiche;
- generazione e pubblicazione degli articoli.

Questa informatica *pret-a-porter*, però, ha il difetto di produrre siti stilisticamente uniformi, che funzionano bene soltanto nel caso di esigenze editoriali banali: inserimento di un articolo, redazione e pubblicazione.  
Qualunque deviazione dalla norma (inteso nel senso statistico del termine), richiede un ampliamento delle capacità del sistema e qui, come direbbe Shakespeare, comincia il male.  

## I plug-in

Come dice il nome, i *plug-in* sono dei componenti aggiuntivi che si caricano all'interno del CMS e ne ampliano le capacità.  
In questi giorni, sto realizzando il sito Web di una associazione politica che deve permettere il pagamento *on-line* della quota associativa. 
Il sito è realizzato in Wordpress e devo quindi scegliere un plug-in che gestisca il colloquio con il sistema di pagamento. 
L'elenco dei plug-in Wordpress per Paypal comprende al momento più di 1.000 prodotti; per ciascun prodotto, l'elenco riporta il numero di installazioni, un indice di gradimento in stelline e il numero di recensioni ricevute.  
Come possiamo scegliere quello giusto?  

<a class="img" href="/assets/img/2022/plugin-paypal-1.jpg">
    <img class="illustrazione cornice" src="/assets/img/2022/plugin-paypal-1.jpg" alt="Elenco dei plugin Pay-Pal" title="Fate click sull'immagine per ingrandirla">
</a>

Nel mio caso, ho cercato dei plugin che avessero un buon indice di gradimento, un alto numero di installazioni (ma non *troppo* alto, in modo da non essere particolarmente appetibili dagli hacker) e degli aggiornamenti recenti.
Ho trovato questi due:

<a href="/assets/img/2022/plugin-paypal-2.jpg">
<img class="illustrazione cornice" src="/assets/img/2022/plugin-paypal-2.jpg" alt="I plugin Pay-Pal selezionati" title="Fate click sull'immagine per ingrandirla">
</a>

Se il test post-installazione non evidenzierà particolari differenze fra i due plugin, penso che sceglierò quello di sinistra, perché è prodotto da una società, mentre quello di destra è opera di un singolo sviluppatore. 
Questo non è, in sé, una garanzia di qualità (anzi), ma mi dà maggiori garanzie di continuità sul lungo periodo.  
I webmaster *pret-a-porter*, però, non si fanno tutti questi problemi e installano il primo plugin che trovano, senza preoccuparsi né della sua validità né della sicurezza. 
I dati del plugin di WooCommerce illustrano chiaramente il problema: più di mezzo milione di installazioni, ma un indice di gradimento bassino. 
Relativamente alla sicurezza, le cose non vanno meglio: quello qui sotto è l'elenco delle vulnerabilità (note) dei prodotti WooCommerce:

<a href="/assets/img/2022/cve-woocommerce.jpg">
<img class="illustrazione cornice" src="/assets/img/2022/cve-woocommerce.jpg" alt="Elenco delle vulnerabilità di Woocommerce" 
title="Fate click sull'immagine per ingrandirla" >
</a>

In sostanza: il plugin più usato è quello che sarebbe bene non utilizzare. Astuto.    

## Impressionismo informatico

Possiamo paragonare l'avvento dei CMS alla produzione di colori per pittura in tubetto, avvenuta intorno alla metà del XIX Secolo.  
Prima di allora, i pittori dovevano prepararsi da sé i loro colori, miscelando i pigmenti con l'olio e conservandoli all'interno di vesciche animali  per evitare che si seccassero.
Questo procedimento dava all'artista il massimo controllo sulla genesi della sua opera, ma allo stesso tempo ne limitava la libertà perché lo costringeva a lavorare sempre all'interno di uno studio. 
I neo-pittori, inoltre, avevano la necessità di imparare la tecnica di lavorazione dei colori, che apprendevano o frequentando un'Accademia o andando “a bottega” da un pittore più esperto.
Da un certo punto di vista, questo era un bene, perché permetteva di tramandare le tecniche pittoriche fra una generazione e l'altra, ma da un punto di vista creativo era spesso un grosso ostacolo perché, specie nelle Accademie, lasciava poco spazio all'innovazione.  
Il colore in tubetti diede ai pittori (a olio, gli acquerellisti non avevano di questi problemi) la possibilità di dipingere dove desideravano: *en plein aire*, fra la folla di un bistrot o in una scuola di danza.  

<a href="/assets/img/2022/monet-campo-di-papaveri.jpg">
<img class="illustrazione cornice" src="/assets/img/2022/monet-campo-di-papaveri.jpg" alt="Monet, Campo di papaveri" 
title="Fate click sull'immagine per ingrandirla" >
</a>

Non a caso, Renoir disse che:

> Senza tubetti di colore non ci sarebbero stati Cezanne, Monet, Sisley o Pissarro.

Il tubetto di colore liberò ii pittori dall'obbligo dello studio inteso come luogo geografico - e questo fu un bene -, ma lo liberò anche dallo studio inteso come percorso di apprendimento e questo fu un male, perché il contrappasso di questa conquista fu l'accesso indiscriminato di orde di cialtroni a un'area che, fino ad allora, era stata protetta dalle alte mura dell'apprendistato.  
Parafrasando Renoir, potremmo dire che:  

> Senza tubetti di colore non avremmo avuto Novella Parigini


Un effetto simile lo hanno avuto anche altre innovazioni tecnologiche che hanno semplificato dei processi creativi: 
le **tastiere elettroniche** hanno trasformato in *pianista di piano bar* chiunque riesca a spingere dei tasti con le dita; 
grazie alle **macchine fotografiche digitali**, ogni massaia può rivaleggiare oggi con Cartier-Bresson; 
come la fata di Cenerentola, i **CMS** hanno magicamente trasformato in *webmaster* delle zucche prive di qualunque competenza informatica.  
Non fraintendetemi: con questo non voglio dire che *chiunque* utilizzi un CMS sia un cialtrone, ma solo che i CMS spianano una strada che sarebbe meglio lasciare impervia.
Se qualcuno fa una brutta foto, non ci crea più che un fastidio momentaneo; se un musico improvvisato suona male o storpia le parole di una canzone, tutt'al più ci può rovinare l'aperitivo; ma se uno pseudo-webmaster crea un sito Web vulnerabile, diventa un pericolo per tutti noi.  
  

## Numeri in mia vita

I dati che riporto di seguito sono presi da un ben documentato rapporto, pubblicato due settimane fa, dal sito `codeinwp.com`<a href="#tn-1" class="nota" name="fn-1">1</a>: 

- Worpress detiene il 64% del mercato dei CMS, che corrisponde a poco meno del 43% di tutti i siti Web del Mondo<a href="#tn-2" class="nota" name="fn-2">2</a>. 

- Il 32% della *Top 1 Milion* dei siti Web mondiali, è realizzato con Wordpress<a href="#tn-3" class="nota" name="fn-3">3</a>. 

- Una ricerca di Sucuri ha rivelato che il 60.04% dei siti analizzati conteneva almeno una *backdoor*; il 95.62% di questi siti era realizzato con WordPress<a href="#tn-4" class="nota" name="fn-4">4</a>.

- Il 52% dei siti Wordpress analizzati da Sucuri conteneva codice non aggiornato<a href="#tn-4" class="nota" name="fn-4">4</a>.

- Wordfence segnala 90.000 attacchi a siti WordPress ogni minuto<a href="#tn-5" class="nota" name="fn-5">5</a>.

Le vulnerabilità di Wordpress sono così distribuite<a href="#tn-6" class="nota" name="fn-6">6</a>:

- 80.2% file di sistema;
- 18.8% plugin;
- 2% temi.

<a href="assets/img/2022/vulnerabilita-wp.png">
<a href="/assets/img/2022/vulnerabilita-wp.png"><img class="illustrazione cornice" src="/assets/img/2022/vulnerabilita-wp.png" alt="Distribuzione delle vulnerabilità Wordpress" ></a>
</a>

In altre parole, **i CMS - e in particolare Wordpress - hanno trasformato Internet in un campo minato**.   
In base alle percentuali viste sopra, su cento siti che visitate:

    43 sono realizzati con CMS;
    25 contengono del codice malevolo;  
    28 sono realizzati con Wordpress;
    14 non hanno codice aggiornato;
     2 sono stati violati per password non sicura.

La vulnerabilità dei sistemi realizzati con CMS non è un problema solo per i gestori del sito e per i suoi visitatori, ma per tutti noi, perché spesso questi siti sono utilizzati per ospitare pagine di *phishing* o di *SPAM* oppure meccanismi per la diffusione di *ransomware*.  
Appartenendo a siti leciti, le URL di queste pagine malevole non sono bloccate (inizialmente) dai sistemi di controllo della posta elettronica e raggiungono quindi più facilmente le loro vittime.
Se fra queste di loro c'è un malaccorto dipendente di un apparato statale, l'inesperienza del webmaster può danneggiare diverse migliaia di persone. 

## La porno-abbazia

Una decina di ani fa, un amico mi chiese di verificare il suo server Web perché, a suo dire, non funzionava più.    
In realtà, il server funzionava benissimo, solo, non aveva più spazio disco perché gli erano tornati indietro più di due milioni di messaggi di SPAM inviati a indirizzi non esistenti o non attivi.  
Un plugin vulnerabile e delle credenziali di accesso disneyane (utente: *vito*; password: *vito*) avevano permesso a due gruppi di hacker - uno dei quali presumibilmente indonesiano, data la presenza di un file con nome *ganteng*, che pare sia l'equivalente del nostro: *gajardo* - di utilizzare il server per l'invio di milioni di messaggi di SPAM.  
Prima di ripulire il disco, salvai l'elenco delle URL malevole contenute nei messaggi e le segnalai a *Project Honey Pot*<a href="#tn-8" class="nota" name="fn-8">8</a>, con cui collaboravo.  
Ingenuamente, pensai di avvisare personalmente i responsabili dei siti con dominio ".it", ma mi resi conto ben presto che non era una cosa fattibile. 
Come puoi segnalare all'Abbazia di Montecassino che il loro sito Web contiene una pagina di rimando a un sito porno? 
Chi avvisi? Come? 
Adesso Montecassino ha un sito Joomla con un template *hand-made* da una società locale, ma allora non aveva nemmeno un indirizzo di posta elettronica; l'unico riferimento era il numero di telefono (fisso) di tale *Don Mario*.  
Problemi simili, con l'aggravante del grottesco, c'erano anche per la segheria bergamasca o per l'associazione femminista, così rinunciai. 
Rimane però il fatto che una leggerezza da parte del mio amico Vito ha causato un potenziale pericolo per milioni di persone.

## Contromisure

Ho scritto altrove che segnalare un problema senza indicare una soluzione è fare allarmismo, non informazione; al momento, però, non riesco a immaginare una soluzione realmente praticabile al problema dell'uso malaccorto dei CMS.
Con più di cinquecento nuovi siti creati ogni giorno e ventisette post pubblicati *al secondo* (per il solo Wordpress) la vedo un'opzione improbabile come cercare di regolamentare la qualità della musica nei bar.
L'unica cosa che possiamo fare, per cercare di arginare il rischi, è di definire delle buone prassi e diffonderle il più possibile, nell'interesse di tutti.  

### Sistema

Un buon modo per rendere i CMS più sicuri è di rendere più sicuri i CMS:

- I gestori dei siti Web devono **mantenere aggiornato il software** dei loro sistemi, installando sempre le versioni (stabili) più recenti dei CMS, dei plugin e dei temi.

- Un'altra buona idea è di __mantenere aggiornato anche il *middleware*__ , come PHP o Apache.  

 - Dal canto loro, gli sviluppatori dei CMS devono **produrre codice sicuro** e devono **correggere rapidamente le vulnerabilità note** dei sistemi.


### Plugin 

Dato che più del 50% delle compromissioni avviene a causa dei plugin<a href="#tn-8" class="nota" name="fn-8">8</a>, è bene definire delle buone norme di comportamento anche per il loro utilizzo.  

<a href="/assets/img/2022/cause-compromissione-siti-web.png"><img   class="illustrazione cornice" 
    src="/assets/img/2022/cause-compromissione-siti-web.png" 
    alt="Cause delle compromissioni dei siti Wordpress " >
</a>

- **Usare solo i plugin necessarii**, per aggiungere al sistema delle funzionalità che lui non è in grado di fornire autonomamente, come per esempio, la gestione della sicurezza; se dovete solo cambiare il colore dei titoli, fatelo modificando i CSS o acquistando un tema che lo consenta.
   
- **Scegliere accuratamente i plugin** in base alle regole esposte sopra: diffusione, stabilità, gradimento, vulnerabilità note.  

- **Mantenere sempre aggiornati i plugin**; l'ho già detto, ma è bene ribadirlo.

### Gestione

I sistemi devono essere configurati e gestiti in maniera sicura. 
Ricordatevi che il software funziona bene solo se non gli lasciate altra scelta.

- Scegliete delle **password sicure**, attivate un controllo del numero massimo di tentativi di accesso e, almeno per gli amministratori del sistema, attivate l'**autenticazione a due fattori**.

- **Rimuovete i meta tag** `generator` dall'header delle pagine, in modo da non indicare il tipo e la versione del CMS utilizzato per realizzarle. Se possibile, fatelo (o fatelo fare) a mano, senza utilizzare un plugin. 

- **Modificate la URL di accesso al sistema**; questa forma di *security by obscurity* - così come spostare il file `wp-config.php` in una directory non accessibile dal server www -  non fermerà un cybercriminale russo intenzionato a entrare proprio nel *vostro* sito, ma terrà alla larga gli aspiranti hacker e i pirati informatici che non ce l'hanno con voi.

### Old school

Oltre a tutto questo, ovviamente, dovrete seguire quelle che sono le buone prassi per *qualunque* sito Web: 

- **Assegnate dei privilegi minimi a ciascun utente:** i redattori devono solo pubblicare articoli; i direttori devono poter pubblicare gli articoli e modificarli se necessario; le modifiche alla configurazione del sistema devono essere permesse solo all'amministratore, che deve accedere al sistema in maniera più controllata degli altri utenti.  
- ***Verificate i privilegi delle directory e dei file di sistema***, anche qui in base al principio del *least privilege*.
- **Configurate i _security header HTTP_**<a href="#tn-9" class="nota" name="fn-9">9</a>, verificando sempre il loro corretto funzionamento:
```
> curl --head carlosimonelli.it
HTTP/1.1 200 OK
Etag: 20e8e2d32-25f0-638be838
Content-Type: text/html; charset=utf-8
Content-Length: 9812
Last-Modified: Mon, 21 Nov 2022 21:05:59 GMT
X-Frame-Options: SAMEORIGIN
X-Content-Type-Options: nosniff
X-XSS-Protection: 1; mode=block
Cache-Control: private, max-age=0, proxy-revalidate, no-store, no-cache, must-revalidate
Server: WEBrick/1.8.0 (Ruby/3.1.2/2022-04-12)
Date: Mon, 21 Nov 2022 21:09:30 GMT
Connection: Keep-Alive
```
Eccetera.

## Il sito Di Maio

Il *defacement* del sito di Di Maio è un perfetto esempio di gestione malaccorta di un CMS.  
I pirati informatici avevano fatto due modifiche alla pagina: avevano alterato il contenuto dei meta tag `description` e `og:description`, inserendo un testo che pubblicizzava un casino *on-line* e avevano aggiunto l'immagine di una freccia, che, se cliccata, apriva il sito malevolo (v. <a href="#hack">immagine iniziale</a>).    
Essendo all'interno dell'`HEAD` della pagina, il testo pubblicitario non era visibile aprendo il sito in un browser, ma compariva nei risultati delle ricerche di Google:

<a href="/assets/img/2022/ricerca-google-dimaio.jpg" name="google"><img   class="illustrazione cornice" 
    src="/assets/img/2022/ricerca-google-dimaio.jpg" 
    alt="Il testo malevolo nei risultati di ricerca di Google">
</a>

Non so dire come abbiano fatto gli hacker a violare il sito di Di Maio, se abbiano sfruttato un plugin vulnerabile o se siano riusciti a rubare delle credenziali di accesso al sistema. 
Quello che so è che questa violazione si sarebbe potuta facilmente evitare perché, al momento dell'attacco, i “sito” di Di Maio consisteva di **un'unica pagina HTML**, che conteneva **solo un'immagine di sfondo** e un link al sito di Impegno Civico.  
Il codice HTML necessario a visualizzare una pagina con queste caratteristiche è:

```
<html>
    <head>
        <style>
            body {
                background-image: url(di-maio-bg.jpg);
                background-size: 100%;
            }
            h1 {
                font-size: 84pt;
                color: #fff;
                font-family: monospace;
                position: absolute;
                top: 35%;
                left: 8%;
            }
            p {
                font-size: 18pt;
                color: #fff;
                font-family: monospace;
                position: absolute;
                top: 80%;
                left: 8%;
                padding: 10px 20px;
                background-color: #090;
            }        
            a {
                text-decoration: none;
                color: inherit;
            }
        </style>
    </head>
    <body>
        <h1>Luigi<br />DI MAIO</h1>
        <p>
            <a href="https://www.impegno-civico.it">
                Visita il sito di Impegno Civico
            </a>
        </p>
    </body>
</html>
```

<a href="/assets/img/2022/di-maio-out.jpg">
    <img class="illustrazione cornice" 
    src="/assets/img/2022/di-maio-out.jpg" 
    alt="Output del codice HTML qui sopra">
</a>

È insensato utilizzare un CMS per pubblicare una singola pagina HTML.
È insensato e pericoloso, ma - per ignoranza, leggerezza fretta o pigrizia - è stato fatto, e i cybercriminali ne hanno subito approfittato.

## Cosa c'entra Miley Cyrus

Sono andato indubbiamente lungo con il testo, ma visto che avete avuto la bonomia di leggere fin qui, non me la sento di lasciarvi col dubbio.  
*È tutto un complesso di cose*, come direbbe Paolo Conte, che mi ha portato a ricercare il sito di Luigi Di Maio. 
Avevo appena visto il video di una *cover*, fatta da Miley Cyrus, della canzone *Heart of Glass* dei *Blondie*<a href="#tn-10" class="nota" name="fn-10">10</a>. 
Ripensando ai *Blondie*, mi ero chiesto che fine avesse fatto Debbie Harry e, purtroppo, ho soddisfatto la mia curiosità cercando: “Debbie Harry 2022” su Youtube.  
Mentre cercavo di riprendermi dallo spavento, qualcuno su Facebook ha nominato Di Maio e il mio cervello, alla disperata ricerca di una distrazione, ha fatto una crasi mnemonica generando la domanda: 

> Ma che fine ha fatto, Di Maio?

La successiva ricerca su Google ha prodotto i risultati che vedete nell'immagine <a href="#google">qui sopra</a>, rivelandomi il *defacement*.  
Tutto qui.  

<a href="/assets/img/2022/miley-cyrus-foto-kim-erlandsen.jpg">
    <img class="illustrazione cornice" 
    src="/assets/img/2022/miley-cyrus-foto-kim-erlandsen.jpg" 
    alt="Miley Cyrus - Telenor Arena 2014. Foto: Kim Erlandsen, NRK P3">
</a>


<section class="note">
    <h2>Note</h2>
    <ol>
        <li>
            <a target="note"
             href="https://www.codeinwp.com/blog/wordpress-statistics/">
             www.codeinwp.com/blog/wordpress-statistics</a>
            <a href="#fn-1" name="tn-1">&#8618;</a>           
        </li>
        <li>
            <a target="note"
             href="https://w3techs.com/technologies/history_overview/content_management/ms/">
             w3techs.com/technologies/history_overview/content_management/ms/</a>
            <a href="#fn-2" name="tn-2">&#8618;</a>            
        </li>
        <li>
            <a target="note"
             href="https://trends.builtwith.com/cms">
             trends.builtwith.com/cms</a>
            <a href="#fn-3" name="tn-3">&#8618;</a>
            
        </li>
        <li>
            <a target="note"
             href="https://sucuri.net/reports/2021-hacked-website-report/">
             sucuri.net/reports/2021-hacked-website-report/</a>
            <a href="#fn-4" name="tn-4">&#8618;</a>
            
        </li>
        <li>
            <a target="note"
             href="https://www.wordfence.com">
             /www.wordfence.com</a>
            <a href="#fn-5" name="tn-5">&#8618;</a>
            
        </li>
        <li>
            <a target="note"
             href="https://www.wpwhitesecurity.com/statistics-highlight-main-source-wordpress-vulnerabilities/">
             www.wpwhitesecurity.com/statistics-highlight-main-source-wordpress-vulnerabilities/</a>
            <a href="#fn-6" name="tn-6">&#8618;</a>
            
        </li>
        <li>
            <a target="note"
             href="https://www.projecthoneypot.org">
             www.projecthoneypot.org
            </a>
            <a href="#fn-7" name="tn-7">&#8618;</a>
            
        </li>
        <li>
            <a target="note"
             href="https://www.wordfence.com/blog/2016/03/attackers-gain-access-wordpress-sites/">
                www.wordfence.com/blog/2016/03/attackers-gain-access-wordpress-sites/
            </a>
            <a href="#fn-8" name="tn-8">&#8618;</a>        
        </li>
        <li>
            <a target="note"
             href="https://www.keycdn.com/blog/http-security-headers/">
                www.keycdn.com/blog/http-security-headers
            </a>
            <a href="#fn-9" name="tn-9">&#8618;</a>        
        </li>
        <li>
            <a target="note"
             href="https://www.youtube.com/watch?v=NbdRLyixJpc">
                https://www.youtube.com/watch?v=NbdRLyixJpc
            </a>
            <a href="#fn-10" name="tn-10">&#8618;</a>        
        </li>
    </ol>    
</section>
