---
category:   "sicurezza"
excerpt:    "Un sincero messaggio di scuse al professor Matteo Flora per il disturbo arrecatogli"
h1:         "De Flora novarum"
h2:         "Un sincero messaggio di scuse al professor Matteo Flora per il disturbo arrecatogli"
layout:     post
image:      "/assets/img/2022/flora-post-twitter.jpg"
thumbnail:  "/assets/img/2022/flora-post-twitter-m.jpg"
img-alt:    "La pagina del sito di Luigi di Maio"
permalink:  "/sicurezza/de-flora-novarum.html" 
tags:       [cybersecurity, Luigi di Maio, Flora, Matteo]
title:      "De Flora Novarum"
---


<section class="citazione">
“Yea, it's like a plumber: do your job right and nobody should notice. But when you fuck it up, everything gets full of shit.”<br />
<span>Dustin Hoffman in:</span> Wag the dog
</section>

Il 12 Ottobre, in maniera del tutto casuale, mi sono accorto di un <a href="https://www.treccani.it/enciclopedia/defacement_%28altro%29/" target="treccani"><i>defacement</i></a>  del sito personale di Luigi di Maio.  
Per evitare una figura barbina, la prima cosa che ho fatto è stata di controllare che la cosa non fosse già di dominio pubblico, ma per quanto l'alterazione fosse ben visibile sia nella pagina che nell'esito delle ricerche su Google, nessuno prima di allora l'aveva segnalata.  

<a href="/assets/img/2022/di-maio-hack.jpg" name="hack" target="img" title="Fare click sull'immagine per ingrandirla"><img class="illustrazione cornice" src="{{ page.image }}" ></a>
<p class="didascalia">La home-page del sito di Di Maio. La freccia sotto la lettera "O" porta alla URL evidenziata nell'angolo in basso a sinistra.</p>

Appurata la novità della notizia, l'ho riportata su Linkedin.. 

<a href="/assets/img/2022/flora-post-linkedin.png" name="hack" target="img" title="Fare click sull'immagine per ingrandirla"><img class="illustrazione cornice" src="/assets/img/2022/flora-post-linkedin.png" ></a>
<p class="didascalia">Il mio post su Linkedin.</p>

..e l'ho segnalata a due giornali con cui collaboro, che l'hanno riportata nei loro articoli: 
<a href href="https://www.globalist.it/world/2022/10/12/di-maio-bucato-il-sito-nella-descrizione-la-pubblicita-dei-casino-online/" target="globalist">Globalist</a>, il 12 Ottobre stesso; 
<a href="https://www.difesaesicurezza.com/cyber/cybercrime-hackerato-il-sito-web-personale-di-luigi-di-maio/" target="des">Difesa & Sicurezza</a>, due giorni dopo.  
Avvisato il Mondo del pericolo, sono uscito a festeggiare la scoperta e non ho più pensato al sito di Di Maio fino a quando non l'ho utilizzato come esempio in <a href="https://bit.ly/3Ge8m2e" target="des" title="Link al mio articolo su Difesa & Sicurezza">un mio articolo</a> sulla pericolosità dei siti realizzati (male) con i *CMS* come Wordpress.  
È stato allora che un amico mi ha detto: “La notizia del sito di Di Maio ricordo che l'aveva data Flora”.  
Flora?    
Una rapida ricerca su Google mi ha permesso di scoprire che “Flora” era un cognome e che la notizia era stata data su Twitter:

<a href="/assets/img/2022/flora-post-twitter.jpg" name="hack" target="img" title="Fare click sull'immagine per ingrandirla"><img class="illustrazione cornice" src="/assets/img/2022/flora-post-twitter.jpg" ></a>
<p class="didascalia">Il post di Matteo Flora su Twitter.</p>

Confesso che leggendo il *tweet* del professor Flora mi sono sentito profondamente in colpa.
Se al momento della scoperta fossi stato meno frettoloso e avessi scritto qualcosa anche su Twitter, non solo gli avrei avrei evitato il fastidio di dare la notizia in mia vece, ma avrei evitato una brutta figura alla categoria degli esperti di sicurezza informatica.  
La realtà è che io, malgrado lavori con Internet dal 1995, ancora non riesco a capire come funziona Twitter.
Davvero.
Per quanto mi sforzi di entrare nella “logica” della sua interfaccia utente, lo trovo contro-intuitivo e lo utilizzo solo quando non posso farne a meno, ovverosia quasi mai.  
La mia incapacità di relazionarmi con i cinguettii digitali spiega il mio comportamento, ma non mi giustifica.
Ho avuto tre giorni per scrivere qualcosa anche su Twitter, ma non l'ho fatto. 
Colpevolmente.
Devo quindi scusarmi con lei, Flora per il fastidio arrecatole e
ci tengo inoltre a rassicurarla sulle competenze degli esperti di sicurezza nostrani: non è che NESSUNO si fosse accorto del *defacement* del sito di Di Maio, è che noi persone della sicurezza siamo - un po' per indole un po' per contratto - scarsamente propensi a parlare di ciò che facciamo.
Come dice la battuta di *Sesso e potere* che ho riportato all'inizio, se svolgiamo bene il nostro lavoro, non se ne accorge nessuno, mentre se ci sbagliamo..  
Non penso succederà di nuovo, ma se per caso dovessi scoprire un altro *defacement* o qualsiasi altra informazione <a href="https://www.first.org/tlp/" target="tpl">TPL:clear</a>, sarà mia cura assoldare un *social-media manager* a cui affiderò il compito di divulgare propriamente la notizia su tutti i canali comunicativi, in modo da risparmiare a lei o ad altri il fastidio di ripetere ciò che io ho già detto in precedenza.  
<br />
Con immutata stima,  
<br />
  Carlo Simonelli 
