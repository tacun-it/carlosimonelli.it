---
category:   "sicurezza"
excerpt:    "Note sul Security Summit 2012"
h1:         "Un romantico a Milano"
h2:         "(per il Security Summit 2012)"
layout:     post
image:      "/assets/img/2012/security-summit.jpg"
thumbnail:  "/assets/img/2012/security-summit-m.jpg"
img-alt:    "Logo del Security Summit"
permalink:  "/security/security-summit-2012.html" 
tags:       [AI, Security Summit, Milano]
title:      "Un romantico a Milano"
---

La prima volta che andai al Security Summit a Milano, fu nel 2009, per ascoltare il resoconto di Gadi Evron sull'attacco DDoS ai danni dell'Estonia
<a href="#estonia" class="nota" name="fn-1">1</a>.

Lavoravo a tempo pieno nella sicurezza da poco più di un anno e anche se ne erano passati venti, dalla prima volta che avevo messo le mani sulla tastiera di un computer e otto, da quando avevo scritto la mia prima funzione per la bonifica dei parametri di input di un sito Web, avevo ancora una visione piuttosto ristretta dei problemi legati alla sicurezza.
Rimasi perciò deluso quando, alla fine del suo intervento, Evron propose come soluzione alla minaccia del crimine informatico, la creazione di una rete di collaborazione mondiale.
Io, che mi aspettavo chissà quale fantascientifica tecnologia o architettura difensiva, pensai:  

“Ma come, mi sono fatto seicento chilometri per sentirmi dire che la parola d'ordine per combattere gli hacker è: *Volémose bene*?”  

Tre anni dopo, con più di centocinquanta test di sicurezza alle spalle e un punto di vista drasticamente più ampio (o *meno stitico*, a seconda dei punti di vista..), sono tornato a Milano per cercare di capire, dall'esperienza di chi ne sa più di me, quali siano le minacce che dobbiamo prepararci a fronteggiare.
È stata un'esperienza piuttosto proficua, che mi ha fornito diverse idee nuove e alcune interessanti conferme:

1. **Gadi Evron aveva ragione:** la cooperazione internazionale è, se non proprio l'unico, sicuramente uno degli strumenti più potenti per opporsi ad attacchi che arrivano da angoli diversi del Pianeta. Come ha dimostrato Bruno Hourdel di Akamai, il modo più efficace di rispondere a un attacco DDos è di combattere nel territorio dell'avversario, senza permettergli di arrivare in casa nostra. Per far questo, abbiamo bisogno di alleati.

2. **Gadi Evron aveva ragione:** la sicurezza dei telefoni cellulari è diventata uno dei temi centrali del nostro lavoro. Tre anni fa, quando lui chiese chi, fra i presenti, avesse messo in atto delle misure di controllo del proprio telefono, si alzarono solo un paio di mani; oggi, molti degli interventi del convegno vertevano sulla sicurezza dei dispositivi *mobile*. Stando così le cose, non posso fare a meno di chiedermi se e quando si avvererà un'altra sua profezia, riguardo gli attacchi agli impianti bionici: *pace-maker*, arti artificiali, eccetera. Mi immagino una scena come quelle nei film di James Bond, con il cattivo che chiama in videoconferenza una multinazionale farmaceutica e chiede un fantastiliardo di Dollari, se no, farà fermare i pace-maker di tutti i loro pazienti. Se mai avvenisse, spero che paghino: ci tengo, a mia madre.

3. **Io avevo ragione:** al pirata informatico "old school", ovvero all'hacker che penetra nel tuo sistema grazie alle sue capacità e alla sua esperienza, sfruttando vulnerabilità applicative complesse come la SQL-injection, si sta affiancando una nuova genìa di pirati di basso profilo, che per i loro attacchi sfruttano tecniche o programmi sviluppati da altri.

Gli hacker *old-style* sono pochi, ma molto pericolosi; i pirati *wannabe* sono mediamente incapaci<a href="#incapaci" class="nota" name="fn-2">2</a>, ma sono tanti, il che li rende altrettanto dannosi, tanto più che i loro bersagli sono casuali: cercano su Google tracce di qualche vulnerabilità nota, poi scatenano sul sito vulnerabile uno strumento di scansione o un *exploit* di cui, magari, non capiscono nemmeno il funzionamento. A meno che l'attaccante non sia così idiota da usare SQLMap senza modificarne lo *user-agent* (càpita..), l'attacco andrà a segno e saranno necessarî mediamente<a href="#mediamente" class="nota" name="fn-3">3</a> diciotto giorni di lavoro per annullarne gli effetti. Un esempio interessante di questa nuova tendenza (non ricordo in quale intervento fosse riportato) è stato il messaggio di un ragazzino, che, in un pessimo Inglese, ricattava il gestore di un forum dicendo: "Ho una botnet di 2000 computer, se non mi mandi 200 Dollari, ti tiro giù il sistema".

D'altro canto, il \[D\]DoS è l'attacco ideale per il pirata *wannabe*, perché non richiede competenze specifiche, ma solo una rete di computer infetti, che può essere presa in affitto a un prezzo tutto sommato conveniente. E mentre dagli altri tipi di attacco si può essere o ci si può rendere immuni, qualunque sistema in linea è inevitabilmente vulnerabile ad attacchi che mirino a ridurre o annullare la sua capacità di rispondere alle richieste.

Stando così le cose, credo sia necessario accettare il fatto che la sicurezza non è più un problema che riguarda solo le banche e i siti delle multinazionali, ma coinvolge tutti i sistemi presenti sulla Internet. Dall'ultimo rapporto di Verizon sui crimini informatici<a href="#verizon" class="nota" name="fn-4">4</a>, risulta che il furto di carte di credito è ancora al primo posto nell'elenco dei tipi di dato compromessi degli attacchi dei pirati informatici, ma al secondo e al terzo posto troviamo, rispettivamente, le credenziali di autenticazione e le informazioni personali. 

<section class="note">
    <h2>Note</h2>
    <ol>
        <li>
            <a href="https://www.youtube.com/watch?v=RnsNz_b-uxE"
               target="note">https://www.youtube.com/watch?v=RnsNz_b-uxE</a>
            <a href="#fn-1" name="estonia">&#8617;</a>
        </li>
        <li>
            <a href="http://www.sciax2.it/forum/assistenza-tecnica/creare-ddos-visual-basci-2008-a-476965.html"
               target="note">http://www.sciax2.it/forum/assistenza-tecnica/creare-ddos-visual-basci-2008-a-476965.html</a>
            <a href="#fn-2" name="incapaci">&#8617;</a>
        </li>
        <li>
            <a href="http://www.sonatype.com/Products/Sonatype-Insight/Why-Insight/Mitigate-Security-Risks/Security-Brief"
               target="note">http://www.sonatype.com/Products/Sonatype-Insight/Why-Insight/Mitigate-Security-Risks/Security-Brief</a>
            <a href="#fn-3" name="mediamente">&#8617;</a>
        </li>
        <li>
            <a href="http://www.youtube.com/watch?v=RnsNz\_b-uxE"
               target="note">http://www.verizonbusiness.com/thoughtleadership</a>
            <a href="#fn-4" name="verizion">&#8617;</a>
        </li>
    </ol>
</section>
