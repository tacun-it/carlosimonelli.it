---
category:   "sicurezza"
excerpt:    "Un approccio nuovo alla sicurezza informatica"
h1:         "Penetration"
h2:         "Un approccio nuovo alla sicurezza informatica"
image:      "/assets/img/2012/coppia-di-gibboni.jpg"
img-alt:    "Coppia di gibboni"
layout:     post
permalink:  "/sicurezza/penetration.html" 
tags:       [sicurezza, Maynard Smith, evoluzione, imene]
thumbnail:  "/assets/img/2012/coppia-di-gibboni-m.jpg"
title:      "Penetration"
---

<section class="citazione">
Un maschio, invece, non copula mai abbastanza con un numero
sufficiente di femmine diverse: per un maschio, la parola *eccesso* non
ha significato.
<span>R. Dawkins</span>, Il Gene Egoista
</section>

Vi siete mai chiesti a cosa serva l'imene?

L'evoluzione ha dotato le femmine di forme e ghiandole atte ad attirare il maschio, di aree fittamente innervate per rendere piacevole e desiderabile il rapporto sessuale, ma, allo stesso tempo, ha frapposto un ostacolo doloroso fra quel desiderio e la sua (prima) soddisfazione.
Perché?

Se non vi siete mai posti il problema, vergognatevi di voi stessi: in quanto esperti di sicurezza, dovreste interessarvi a tutto ciò che impedisce la penetrazione in un apparato; se al contrario, questa apparente contraddizione ha incuriosito anche voi, sappiate che una risposta c'è, e che è perfettamente coerente con i principi evoluzionistici.

Perché un determinato patrimonio genetico possa perpetuarsi, non è sufficiente che uno spermatozoo fecondi l'ovulo: è necessario anche che l'organismo nato da quell'unione cresca e si riproduca a sua volta. 
Se i genitori cooperano allo svezzamento, è più probabile che il neo-nato riceva le cure necessarie a crescere forte e sano; se, invece, i genitori abbandonano la prole, le cose si fanno più difficili.

In uno studio di Maynard Smith, le quattro casistiche possibili sono identificate dal nome della specie animale che le adotta:  

 Specie | Comportamento
---|---|
  Anitra     | il maschio abbandona, la femmina alleva
  Spinarello | la femmina abbandona, il maschio alleva
  Moscerino  | entrambi i genitori abbandonano
  Gibbone    | entrambi i genitori allevano

Ora, la madre, specie nel caso dei mammiferi, ha delle difficoltà oggettive ad abbandonare la prole (se non altro nei primi nove mesi dopo il concepimento), ma cosa spinge il maschio a restare "in famiglia"?

<img class="illustrazione cornice" src="{{ page.image }}" alt="{{ page.img-alt }}">

Parliamoci chiaro: da un punto di vista statistico, il maschio ha più probabilità di perpetuare il suo patrimonio genetico se feconda più femmine. 
Certo, i suoi figli, affidati solo alle cure materne, avrebbero minori probabilità di sopravvivere fino all'età adulta, ma, ponendo che il maschio riesca a fecondare quattro femmine, se anche il 50% della prole non riuscisse a sopravvivere, in capo a nove mesi, resterebbero ancora due copie del suo DNA in circolazione; il doppio di quello che otterrebbe con la monogamia. A lui converrebbe.

La femmina, al contrario, ha tutto l'interesse a far sì che il maschio rimanga: potrà difenderla dai predatori e, soprattutto, potrà occuparsi dei piccoli quando lei va a fare shopping con le amiche. 
Deve perciò accoppiarsi solo con un maschio di comprovata affidabilità, e l'unico modo per verificare l'affidabilità del maschio è di rendergli le cose difficili.

Dawkins, nel libro citato all'inizio, indica le diverse strategie possibili con i termini: *sfacciata*/*ritrosa*, per quanto riguarda le femmine e *fedele*/*seduttore* per quanto riguarda i maschi.

In un mondo in cui tutte le femmine siano *sfacciate*, l'equazione:

    pizzeria + cinema = sesso

spingerà il maschio ad adottare una strategia da *seduttore.*

Se invece i termini dell'equazione fossero:

    sesso =
    aperitivo +
    ristorante +
    teatro +
    diamante +
    fidanzamento +
    casa +
    matrimonio

il maschio dovrebbe pensarci due volte, prima di lasciare il tetto coniugale in cerca di altre femmine: gli converrebbe piuttosto fare un secondo figlio con la sua compagna.

È chiaro che questa tecnica funziona solo se viene adottata dalla maggioranza delle femmine della specie, ma, come dimostra Dawkins, *indipendentemente dal rapporto sfacciate/ritrose nella popolazione femminile iniziale*, è molto probabile che sul lungo periodo le oscillazioni del comportamento ritrosa/sfacciata si stabilizzino in quella che lui chiama una *ESS* (*Evolutionary Stable Strategy*), ovvero una strategia che, se adottata da una parte della popolazione, tende prevalere sulle strategie alternative.

Secondo il calcoli di Dawkins, perché si possa instaurare una simile ESS, nella popolazione femminile devono esserci 5/6 di femmine ritrose e 5/8 di maschi fedeli.

A questo punto, anche i sistemisti AS/400 avranno capito a cosa serva l'imene: rendendo difficile e doloroso il primo rapporto sessuale, fa sì che la maggioranza delle femmine adotti istintivamente quel comportamento ritroso che offre le migliori probabilità di sopravvivenza del patrimonio genetico. 
Come abbiamo visto, però, una volta adottata, la strategia della "femmina ritrosa" si stabilizza inevitabilmente nella popolazione e i poveri maschi, fedeli o seduttori che siano, alla fine devono cominciare a pagare ciò che fino ad allora hanno avuto gratis o a basso costo. 
E qui cominciano i problemi.

Per semplicità, immaginiamo di avere 48 maschi e 48 femmine: se i calcoli di Dawkins sono giusti, la popolazione sarà così suddivisa:

    40 ritrose
     8 sfacciate
    30 fedeli
    18 seduttori

Considerata la tendenza dei seduttori a fruire di più di una donna, è facile vedere quanto sia difficile, per un maschio, ottenere soddisfazione ai suoi legittimi e naturalissimi desideri.

In simili condizioni, è facile ipotizzare che la lotta per le femmine diventi accanita e che solo gli esemplari migliori riescano a prevalere.
Quando ciò avverrà, cosa faranno, gli altri?

Dipende: chi sa scrivere, scriverà un libro o un film sul suo amore travagliato per la ritrosa o la sfacciata di turno, nella speranza che questo gli permetta di conquistarla; chi sa leggere, diventerà critico letterario e cercherà di denigrare i rivali scrittori per sminuirli agli occhi delle femmine residue; chi sa parlare, farà il politico e cercherà nella dominazione sociale un succedaneo alla dominazione carnale.
Chi non sa fare nulla di tutto ciò, molto probabilmente diventerà un pirata informatico.

Non umiliate la vostra intelligenza replicando che, al giorno d'oggi, le proporzioni fra ritrose e sfacciate sono, nella migliore delle ipotesi, invertite, e che la moda e l'atteggiamento delle femmine sono un continuo richiamo sessuale per il maschio. Non è questo il punto.
Cento anni di femminismo e di moda non possono opporsi a dodicimila anni di evoluzione e comunque, rendersi appetibile è qualcosa che le donne hanno sempre fatto. Ciò che ci interessa qui, è ciò che succede quando il maschio abbocca all'amo: cinque volte su sei, la donna si ritira e dice: "Io? non stavo mica pescando!"

In questi casi, il maschio più forte saprà, per istinto o per esperienza, che la ritrosia femminile altro non è che una forma criptica di invito; la sua forte personalità non gli farà nemmeno prendere in considerazione l'ipotesi di un rifiuto e lo spingerà a ritentare l'attacco più e più volte, fino alla vittoria.

L'esemplare più debole o insicuro, però, non riuscirà a vedere questo diniego per quello che è in realtà, e lo leggerà piuttosto come un'ulteriore riprova della sua inadeguatezza. Sentendosi vinto dalla vita, costui vedrà l'elica del suo DNA trasformarsi in un binario morto e, come un santo taoista, abbandonerà ogni interesse per le cose mondane (igiene personale e alimentazione bilanciata in primo luogo), ritirandosi in una buia stanza ronzante di ventole, per mettere in atto la sua vendetta contro il Mondo. Incurante dello scorrere del tempo (le serrande sono abbassate) e dell'alternarsi delle stagioni (un condizionatore mantiene stabile la temperatura), adotterà un'interfaccia a caratteri per la sua stessa esistenza, e si consacrerà alla distruzione di ciò che gli altri hanno costruito.

Ma, chiedetevi: è proprio questo, ciò che vuole? Distruzione e vendetta?
No davvero.

Eroe romantico per eccellenza, come Quasimodo, Dantès e Gatsby, il nostro *black-hat* agisce spinto da una forza ben più grande della semplice vendetta nei confronti di coloro che l'hanno reso un reietto: le sue dita guizzano sulla tastiera nella speranza inconscia (o quanto meno inconfessata) di poter guizzare in seguito, e per ciò, sulle carni di una delle 48 femmine di cui sopra. Potendo, anche più d'una.

Si dice spesso che le motivazioni che spingono gli hacker sono il desiderio di notorietà, i soldi o l'ideologia, ma è un modo molto miope di considerare il problema, ed è precisamente questa miopia che ci ha impedito, fino a ora, di riuscire a risolverlo.
Pensateci bene: perché un uomo desidera i soldi o la fama (sia essa fama di pirata o di paladino)? A cosa servono, davvero, i soldi e la fama, se non a rimorchiare le donne?
Certo, mi si potrà obiettare che esistono degli hacker di bell'aspetto e di buon successo con le donne, ma è un'opposizione labile: il fatto che sia esistito un Marcel Marceau non rende migliore, o meno patetico, il mimo che si esibiva ieri davanti al bar dove stavo prendendo l'aperitivo.

D'altro canto, pensate a cosa fanno *effettivamente* gli hacker:

> alterano programmi e/o sistemi, per accedere a risorse che altrimenti gli sarebbero negate.

Non vi fa venire in mente nulla?

Non a caso, prima, ho utilizzato il termine "criptico", riguardo l'atteggiamento femminile.
L'hacker sa che le donne sono criptiche, e allora studia la crittografia.
Sa che sono impenetrabili, e allora studia le tecniche di intrusione.
Sa che non gli daranno mai nulla gratis, e allora impara a forzare i programmi commerciali.
È una correlazione talmente ovvia che solo un sociologo non riuscirebbe a vederla.

E mentre voi vi fate un bell'esame di coscienza, io concludo questo articolo descrivendovi quella che ritengo essere l'unica soluzione possibile al problema della pirateria informatica. Non un firewall, non un software di scansione, non una costosa consulenza: questi sono solo rimedi sintomatici e non scalfiscono minimamente la causa prima del male.
Ciò di cui abbiamo bisogno davvero è di arrivare alle radici stesse del problema, estirpandolo una volta per tutte, quindi:

<p style="font-size:2rem;margin: 1rem 0;">CONVINCIAMO LE DONNE A DARLA VIA PIÙ FACILMENTE!</p>

Se riusciremo a vincere l'innata ritrosia delle femmine nei confronti della copulazione, elimineremo la causa prima dell'esistenza degli hacker. 
Tutti i 48 maschi della nostra popolazione, belli o brutti che siano, disporranno di una o più donne con cui giacersi e non avranno più né il desiderio né l'occasione di dedicarsi ad attività delittuose o comunque moleste.
Guarderanno alla loro passata misantropia con un misto di tenerezza e rimorso ed esorteranno i loro figli (quelle meravigliose macchine cui hanno affidato la perpetuazione del loro patrimonio genetico) a fare i bravi, se no viene il *black-hat* e se li porta via.
Qualcuno di noi (il sottoscritto per primo) si troverà senza lavoro, ma la cosa non lo preoccuperà più di tanto: un lavoro che gli fornisca i soldi per una pizza e per un cinema lo troverà di certo.
Se siamo fortunati, elimineremo anche i politici e i critici letterari.

Vale la pena di tentare.

<section class="bibliografia">
    <h2>Bibliografia essenziale</h2>
    <ul>
        <li>
            Dawkins, R. (1989) <i>Il Gene egoista</i> -
            Arnoldo Mondadori Editore, Milano.
        </li>
        <li>
            Morris, D. (2006) <i>L'animale donna</i> -
            Arnoldo Mondadori Editore, Milano.
        </li>
        <li>
            Groove Armada (2001) <i>My friend</I> -
            Adam Berg (regia).<BR />
            Questo video musicale, merita un discorso a parte.
            Tre ragazze, in vacanza in un'assolata località marina, la sera vanno
            in discoteca vestite del minimo sindacale. Una di loro, la più carina,
            protagonista del video, rimorchia un ragazzo, balla con lui, pomicia su
            un divanetto, poi se ne torna in albergo con le amiche senza curarsi
            minimamente, la stronza, di quel povero cristo che ha sedotto e
            abbandonato.
            Ha avuto un'ulteriore, pleonastica, dimostrazione della sua avvenenza e
            questo le basta; non pensa che il maschietto a cui ha permesso di
            carezzarle il pancino, quella sera dovrà ingaggiare una lunga e faticosa
            *serpesmachìa* per molcire e ricondurre alla ragione una specifica parte
            del suo corpo.
            Asservita ai dettami della moda e alle usanze del suo tempo, la femmina
            della specie ha messo in mostra le sue piume, ma al momento di
            "finalizzare il gioco", come direbbe un commentatore sportivo, ha
            applicato la strategia della femmina ritrosa e, novella Cenerentola, ha
            lasciato in Nasso il suo principe azzurro, senza nemmeno il buon gusto
            di lasciargli una scarpina d'argento come souvenir.
            Ora, io non voglio con questo giustificare gli odiosi abusi del corpo
            femminile, ma, al contrario, desidero chiarire, una volta per tutte, che
            determinati stimoli hanno un inevitabile effetto sul corpo di un
            esemplare maschio sano e che la reiterazione di detti stimoli, senza
            un'adeguata conclusione, può avere effetti imbarazzanti e/o dolorosi
            per il soggetto.
            Sono decisamente contro la violenza carnale, ma in un'accezione
            biunivoca del termine.
        </li>
    </ul>
</section>
